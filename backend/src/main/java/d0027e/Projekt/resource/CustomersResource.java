package d0027e.Projekt.resource;

import d0027e.Projekt.exception.ResourceNotFoundException;
import d0027e.Projekt.model.Customers;
import d0027e.Projekt.model.PictureTest;
import d0027e.Projekt.repo.CustomersRepository;
import d0027e.Projekt.repo.PictureRepo;
import d0027e.Projekt.service.CustomersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EnableAutoConfiguration
@RestController
@RequestMapping("/customers")
@CrossOrigin(origins = "http://localhost:4200")
@Resource
public class CustomersResource {

    private final CustomersService customersService;

    @Autowired
    public CustomersResource(CustomersService customersService) {
        this.customersService = customersService;
    }

    public CustomersRepository customersRepository;

    @GetMapping("/all") //Method to find and load customers from entity database to table
    public ResponseEntity<List<Customers>> getAllCustomers(){
        List<Customers>customers = customersService.findAllCustomers();
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Long> get(@PathVariable("id") Long id) {
        List<Customers>customers = customersService.getCustomersById(id);
        return new ResponseEntity(customers, HttpStatus.OK);
    }

    //Delete entity based on id
    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable("id") Long id) {
        customersService.deleteCustomers(id);
        return new ResponseEntity(id, HttpStatus.OK);
    }

    //Post is when adding something new or updating data
   /* @PostMapping("/{id}")
    public ResponseEntity addCustomers(@RequestBody Customers customers) {
        Customers newCustomers = customersService.addCustomers(customers);
        return new ResponseEntity<>(customers, HttpStatus.CREATED);
    }*/
}
