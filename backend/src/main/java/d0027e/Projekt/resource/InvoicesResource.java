package d0027e.Projekt.resource;

import d0027e.Projekt.exception.ResourceNotFoundException;
import d0027e.Projekt.model.Customers;
import d0027e.Projekt.model.Invoices;
import d0027e.Projekt.model.PictureTest;
import d0027e.Projekt.repo.InvoicesRepository;
import d0027e.Projekt.repo.PictureRepo;
import d0027e.Projekt.service.InvoicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EnableAutoConfiguration
@RestController
@RequestMapping("/invoices")
@CrossOrigin(origins = "http://localhost:4200")
@Resource
public class InvoicesResource {

    private final InvoicesService invoicesService;

    @Autowired
    public InvoicesResource(InvoicesService invoicesService) {
        this.invoicesService = invoicesService;
    }

    public InvoicesRepository invoicesRepository;

    @GetMapping("/all") //Method to find and load customers from entity database to table
    public ResponseEntity<List<Invoices>> getAllInvoices(){
        List<Invoices>invoices = invoicesService.findAllInvoices();
        return new ResponseEntity<>(invoices, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Long> get(@PathVariable("id") Long id) {
        List<Invoices>invoices = invoicesService.getInvoicesById(id);
        return new ResponseEntity(invoices, HttpStatus.OK);
    }

    //Delete entity based on id
    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable("id") Long id) {
        invoicesService.deleteInvoices(id);
        return new ResponseEntity(id, HttpStatus.OK);
    }
}
