package d0027e.Projekt.resource;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import d0027e.Projekt.model.PictureTest;
import d0027e.Projekt.repo.PictureRepo;
import d0027e.Projekt.response.Response;
import d0027e.Projekt.service.FileService;
import d0027e.Projekt.service.PictureServiceImpl;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;


@RestController
@RequestMapping(path="picture", produces="application/json")
@CrossOrigin(origins = "http://localhost:4200")


public class PictureResource {




    @Autowired
    private FileService fileService;
    @Autowired
    private PictureServiceImpl pictureServiceImpl;


    @Autowired
    public PictureRepo pictureRepo;
    @Autowired
    ServletContext context;
    String FILE_PATH_ROOT="C:/Users/irina/Downloads/Database_Pictures/backend/src/main/webapp/images/";

    public PictureResource(FileService fileService) {
        this.fileService = fileService;
    }


    /**@GetMapping("/all")
     public ResponseEntity<List<PictureTest>> getAllPictures(@RequestParam(required = false) String title) {
         try {
             List<PictureTest> pictures = new ArrayList<PictureTest>();
             if (title == null)
                 pictureRepo.findAll().forEach(pictures::add);
             else
                 pictureRepo.findPictureByTitle(title);
             if (pictures.isEmpty()) {
                 return new ResponseEntity<>(HttpStatus.NO_CONTENT);
             }
             return new ResponseEntity<>(pictures, HttpStatus.OK);
         } catch (Exception e) {
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }**/

 /** @GetMapping("/find_1/{title}")
    public ResponseEntity<List<PictureTest>> getPictureByTitle(@RequestParam(required = false) String title) {
        try {
            List<PictureTest> pictures = new ArrayList<PictureTest>();
            if (title == null)
                pictureRepo.findAll().forEach(pictures::add);
            else
                pictureRepo.findPictureByTitle(title).forEach(pictures::add);
            if (pictures.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(pictures, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }**/
  /**  @GetMapping("/find/{id}")
    public ResponseEntity<PictureTest> getPictureById(@PathVariable("id") Long id) {
    Optional<PictureTest> pictureData = pictureRepo.findById(id);
    if (pictureData.isPresent()) {
    return new ResponseEntity<>(pictureData.get(), HttpStatus.OK);
    } else {
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    }**/


  /** @GetMapping("/find/{id}")
     public ResponseEntity<PictureTest> getPictureById(@PathVariable("id") Long id){
         PictureTest picture = (PictureTest) pictureService.findPictureById(id);
         return new ResponseEntity<>(picture, HttpStatus.OK);
     }**/

  @GetMapping("/find/{id}")
    public ResponseEntity<PictureTest> findById(@PathVariable("id") long id) {
      Optional<PictureTest> pictureData = pictureRepo.findById(id);
      if (pictureData.isPresent()) {
          return new ResponseEntity<>(pictureData.get(), HttpStatus.OK);
      } else {
          return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
  }

       /** @GetMapping("/find_1/{title}")
    public ResponseEntity <PictureTest> getPictureByTitle(@PathVariable("title") String title){
        PictureTest picture = pictureService.findPictureByTitle(title);
        return new ResponseEntity<>(picture, HttpStatus.OK);
    }**/

     /**@GetMapping("/find_2/{id}")
     @ResponseBody

       void findPictureById (@PathVariable("id") Long id, HttpServletResponse response)
            throws ServletException, IOException {

            final Optional <PictureTest> pictures = pictureRepo.findPictureById(id);
            response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
            response.getOutputStream().write(pictures.get().getPicByte());
            response.getOutputStream().close();/



      }**/

    @GetMapping("/find_1/{title}")
    public List findPictureByTitle (@PathVariable("title") String title) {

        List pictures = pictureRepo.findPictureByTitle(title);
        return pictures;
    }

    @GetMapping("/find_2/{cathegory}")
    public List findPictureByCathegory (@PathVariable("cathegory") String cathegory) {

        List pictures = pictureRepo.findPictureByCathegory(cathegory);
        return pictures;
    }


   /** @GetMapping(path = { "/get/{title}" })
     public PictureTest findImageByTitle (@PathVariable("title") String title) throws IOException {
       final Optional <PictureTest> retrievedImage = pictureRepo.findImageByTitle(title);
        PictureTest img = new PictureTest(retrievedImage.get().getTitle(),
                   retrievedImage.get().getType(), decompressBytes(retrievedImage.get().getPicByte()));
         return img;
            }**/


    /**@GetMapping("/find_1/{title}")
    public List findPictureByTitle (@PathVariable("title") String title) {

        List pictures = pictureRepo.findPictureByTitle(title);
        return pictures;
    }**/


    // uncompress the image bytes before returning it to the angular application
    public static byte[] decompressBytes(byte[] data) {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException ioe) {
        } catch (DataFormatException e) {
        }
        return outputStream.toByteArray();
    }

   /** @GetMapping("/find_1/{title}")
    public ResponseEntity<List<PictureTest>>getPictureByTitle(@RequestParam (required=false) String title){
        try{
            List <PictureTest> pictures=new ArrayList<PictureTest>();
            if(title==null){
                pictureRepo.findAll().forEach(pictures::add);
            }
            else
                pictureRepo.findPictureByTitle(title);

        return new ResponseEntity<>(pictures, HttpStatus.OK);}
        catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
    }**/

  /** @PostMapping("/add5")
    public FileUploadResponse addPicture(@RequestParam(value="file" , required = false)  MultipartFile file) throws IOException {
        PictureTest picture = fileService.addPicture(file);
        FileUploadResponse response = new FileUploadResponse();
        if(picture!=null){
            String downloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/picture/all1/")
                    .path(picture.getTitle())
                    .toUriString();
            response.setDownloadUri(downloadUri);
            response.setFileId(picture.getTitle());
            response.setFileType(picture.getType());
            response.setUploadStatus(true);
            response.setMessage("File Uploaded Successfully!");
            return response;

        }
        response.setMessage("Oops 1 something went wrong please re-upload.");
        return response;
    }**/



  /**  @RequestMapping(value="/add", method = RequestMethod.POST, consumes ={ "multipart/form-data"} , produces = { "application/json", "application/xml" })




    public ResponseEntity.BodyBuilder addPictures(@RequestParam ("file") MultipartFile file) throws IOException {

        System.out.println("Original Image Byte Size - " + file.getBytes().length);
        //PictureTest pictures=new PictureTest();
       PictureTest img = new PictureTest(pictures.getId(), pictures.getTitle(), pictures.getDescription(), pictures.getCameraType(),
                pictures.getPictureDate(), pictures.getCathegory(), pictures.getKeyword(), pictures.getWidth(),
                pictures.getHeight(), pictures.getMaxAmountPublic(), pictures.getSalesPrice(), pictures.getGps_latitude(),
                pictures.getGps_longitude(), compressBytes(file.getBytes()), pictures.getPicSize());
        pictureRepo.save(img);**/
       /** PictureTest img = new PictureTest(file.getOriginalFilename(), file.getContentType(),
                compressBytes(file.getBytes()));
        pictureRepo.save(img);
        return ResponseEntity.status(HttpStatus.OK);
    }**/
   // String cathegory, String keyword, int width, int heght, int
  //  maxAmountPublic, int salesPrice, String gps_latitude, String gps_longitude, String type, byte[] picByte, Long picSize

    // compress the image bytes before storing it in the database
    /**  public static byte[] compressBytes(byte[] data) {
       Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
        }
        System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);

        return outputStream.toByteArray();
    }**/

    @GetMapping("/all")
public ResponseEntity<List<PictureTest>> getAllPictures(){
List<PictureTest>pictures = pictureServiceImpl.findAllPictures();
return new ResponseEntity<>(pictures, HttpStatus.OK);
}

    @PostMapping("/add")
    public ResponseEntity<Response> addPicture (@RequestParam("file") MultipartFile file, @RequestParam("picture") String picture) throws JsonParseException, JsonMappingException, Exception
    {
        System.out.println("Ok .............");
        PictureTest pic = new ObjectMapper().readValue(picture, PictureTest.class);
        boolean isExit = new File(FILE_PATH_ROOT).exists();
        if (!isExit)
        {
            new File (FILE_PATH_ROOT).mkdir();
            System.out.println("mk dir.............");
        }
        String filename = file.getOriginalFilename();
        String newFileName = FilenameUtils.getBaseName(filename)+"."+FilenameUtils.getExtension(filename);//filename gick bra
        File serverFile = new File (FILE_PATH_ROOT+File.separator+newFileName);

        String type = file.getContentType();
        byte[]picByte=file.getBytes();
        //String newType = FilenameUtils.getBaseName(type)+"."+FilenameUtils.getExtension(type);//type
        //File serverFile1 = new File (context.getRealPath("/images/"+File.separator+newType));

        try
        {
            System.out.println("picture");
            FileUtils.writeByteArrayToFile(serverFile,file.getBytes());

        }catch(Exception e) {
            e.printStackTrace();
        }
        pic.setName(newFileName);
        pic.setType(type);
        pic.setPicByte(picByte);
        PictureTest pics = pictureRepo.save(pic);
        if (pics!= null)
        {
            return new ResponseEntity<Response>(new Response ("OKOK"),HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<Response>(new Response ("Post not saved"),HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping ("/getAll")
    public ResponseEntity<List<String>> getAll()
    {
        List<String> listArt = new ArrayList<String>();
        String filesPath = FILE_PATH_ROOT;
        File filefolder = new File(filesPath);
        if (filefolder != null)
        {
            for (File file :filefolder.listFiles())
            {
                if(!file.isDirectory())
                {
                    String encodeBase64 = null;
                    try {
                        String extension = FilenameUtils.getExtension(file.getName());
                        FileInputStream fileInputStream = new FileInputStream(file);
                        byte[] bytes = new byte[(int)file.length()];
                        fileInputStream.read(bytes);
                        encodeBase64 = Base64.getEncoder().encodeToString(bytes);
                        listArt.add("data:image/"+extension+";base64,"+encodeBase64);
                        fileInputStream.close();


                    }catch (Exception e){

                    }
                }
            }
        }
        return new ResponseEntity<List<String>>(listArt,HttpStatus.OK);

}





   @PostMapping("/add4")
    public ResponseEntity<String> saveProfilePhoto(MultipartFile file) {
        String body = "MultipartFile";
        if (file == null) {
            body = "Null MultipartFile";
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }



   /** @GetMapping("/Img/{title}")
    public ResponseEntity <byte[]> getPhoto(@PathVariable("title") String title) throws Exception{
        byte[] image = new byte[0];

        try {
            image = FileUtils.readFileToByteArray(new File(FILE_PATH_ROOT+title+".jpg"));
        } catch (IOException e) {
            throw new Exception();
        }
        return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(image);
    }**/

   @GetMapping(path="/Img/{id}")
    public byte[] getPhoto(@PathVariable("id") Long id) throws Exception{
        PictureTest picture   = pictureRepo.findById(id).get();
        return Files.readAllBytes(Paths.get(FILE_PATH_ROOT+picture.getName()));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<PictureTest> updatePicture(@PathVariable("id") long id, @RequestBody PictureTest picture) {
        Optional<PictureTest> pictureData = pictureRepo.findById(id);

        if (pictureData.isPresent()) {
            PictureTest _picture = pictureData.get();
            _picture.setTitle(picture.getTitle());
            _picture.setDescription(picture.getDescription());
            _picture.setCathegory(picture.getCathegory());
            _picture.setKeyword((picture.getKeyword()));
            return new ResponseEntity<>(pictureRepo.save(_picture), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
        @DeleteMapping("/delete/{id}")
        public ResponseEntity<HttpStatus> deletePicture(@PathVariable("id") long id) {
            try {
                pictureRepo.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }
    }





