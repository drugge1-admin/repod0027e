package d0027e.Projekt.dto;
import com.google.gson.annotations.Expose;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class CustomersResponseDTO {

    @NotNull
    @Expose
    @NotBlank
    private String company, contact, email;

    @NotNull
    @Expose
    private CustomersDTO customers;

    @NotNull
    @Expose
    private List<CustomersDTO> listOfAllCustomers;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String contact() {
        return contact;
    }

    public void setContact(String endDate) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CustomersDTO getCustomers() {
        return customers;
    }

    public void setCustomers(CustomersDTO customers) {
        this.customers = customers;
    }

    public List<CustomersDTO> getListOfAllCustomers() {
        return listOfAllCustomers;
    }
}
