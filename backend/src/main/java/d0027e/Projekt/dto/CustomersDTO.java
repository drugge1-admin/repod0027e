package d0027e.Projekt.dto;

import com.google.gson.annotations.Expose;
import d0027e.Projekt.model.Customers;

import javax.validation.constraints.NotNull;



public class CustomersDTO {
    @NotNull
    @Expose
    private String contact, company, email;

    @NotNull
    @Expose
    private Long id;

    @Expose
    private Long discount;

    public CustomersDTO(Customers customers) {
        if (customers != null) {
            this.setId(customers.getId());
            this.setContact(customers.getContact());
            this.setCompany(customers.getCompany());
            this.setEmail(customers.getEmail());
            this.setDiscount(customers.getDiscount());
        }
    }

    public CustomersDTO() {}

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getDiscount() {
        return discount;
    }

    public void setDiscount(Long discount) {
        this.discount = discount;
    }
}
