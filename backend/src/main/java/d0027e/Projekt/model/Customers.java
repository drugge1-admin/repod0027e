package d0027e.Projekt.model;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import com.google.gson.annotations.Expose;


@Entity
@Table(name = "customers")

public class Customers {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)

    @Column(name = "id")
    @Expose
    private Long id;

    @Column(name = "company")
    @Expose
    private String company;

    @Column(name = "contact")
    @Expose
    private String contact;

    @Column(name = "email")
    @Expose
    private String email;

    @Column(name = "discount")
    @Expose
    private Long discount;

    public Customers() {

    }

    public Customers(Long id, String company, String contact, String email, Long discount) {
        super();
        this.id = id;
        this.company = company;
        this.contact = contact;
        this.email = email;
        this.discount = discount;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getDiscount() {
        return discount;
    }

    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {

        String customers = "\nid: " + this.id +
                ", \ncompany " + this.company +
                ", \ncontact: " + this.contact +
                ", \nemail: " + this.email +
                ", \ndiscount: " + this.discount;
        return customers;
    }
}
