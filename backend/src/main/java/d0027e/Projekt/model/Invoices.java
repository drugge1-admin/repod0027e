package d0027e.Projekt.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "invoices")

public class Invoices implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    @Column(name = "id")
    private Long id;

    @Column(name = "company")
    private String company;

    @Column(name = "name")
    private String name;

    @Column(name = "date")
    private Date date;

    @Column(name = "cost")
    private Long cost;

    @Column(name = "state")
    private Boolean isPaid;

    public Invoices() {}

    public Invoices(Long id, String company, String name, Date date, Long cost, Boolean isPaid) {
        super();
        this.id = id;
        this.company = company;
        this.name = name;
        this.date = date;
        this.cost = cost;
        this.isPaid = isPaid;

    }
    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Boolean getState() {
        return isPaid;
    }

    public void setState(Boolean isPaid) {
        this.isPaid = isPaid;
    }


    @Override
    public String toString() {

        String invoices = "\nid: " + this.id +
                ", \ncompany " + this.company +
                ", \nname: " + this.name +
                ", \ndate: " + this.date +
                ", \ncost: " + this.cost +
                ", \nisPaid: " + this.isPaid;

        return invoices;
    }
}

