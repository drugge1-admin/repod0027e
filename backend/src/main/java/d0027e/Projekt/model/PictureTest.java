package d0027e.Projekt.model;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.zip.Deflater;


@Entity
public class PictureTest implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "title")
    private String title;
    private String description;
    private String cameraType;
    private Date pictureDate;
    private String cathegory;
    private String keyword;
    private int width;

    private String gps_latitude;
    private String gps_longitude;


    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getMaxAmountPublic() {
        return maxAmountPublic;
    }

    public void setMaxAmountPublic(int maxAmountPublic) {
        this.maxAmountPublic = maxAmountPublic;
    }

    public int getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(int salesPrice) {
        this.salesPrice = salesPrice;
    }

    private int height;
    private int maxAmountPublic;
    private int salesPrice;



    public String getGps_latitude() {
        return gps_latitude;
    }

    public void setGps_latitude(String gps_latitude) {
        this.gps_latitude = gps_latitude;
    }

    public String getGps_longitude() {
        return gps_longitude;
    }

    public void setGps_longitude(String gps_longitude) {
        this.gps_longitude = gps_longitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCameraType() {
        return cameraType;
    }

    public void setCameraType(String cameraType) {
        this.cameraType = cameraType;
    }

    public Date getPictureDate() {
        return pictureDate;
    }

    public void setPictureDate(Date pictureDate) {
        this.pictureDate = pictureDate;
    }

    public String getCathegory() {
        return cathegory;
    }

    public void setCathegory(String cathegory) {
        this.cathegory = cathegory;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Column(name = "type")
    private String type;

    //image bytes can have large lengths so we specify a value
    //which is more than the default length for picByte column
    @Column(name = "picByteNy", length = 1000)
    @Lob
    private byte[] picByte;

    public byte[] getPicByte(MultipartFile file)  throws IOException{

        byte[] picByte = compressBytes(file.getBytes());
        return picByte;
    }
    @Column(name = "picSize")
    private Long picSize;

    public Long getPicSize(MultipartFile file) throws IOException {
        System.out.println("Original Image Byte Size - " + file.getBytes().length);
        Long picSize = file.getSize();

        return picSize;
    }
    @Column (name="name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPicSize(Long picSize) {
        this.picSize = picSize;
    }

    public String getType(MultipartFile file) throws IOException{

        String type = file.getContentType();
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }



    public void setPicByte(byte[] picByte) {
        this.picByte = picByte;
    }


    public PictureTest() {
    }


    public PictureTest(Long id, String title, String description, String cameraType, Date pictureDate, String cathegory, String keyword, int width, int height, int
            maxAmountPublic, int salesPrice, String gps_latitude, String gps_longitude, byte[] picByte, Long picSize) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.cameraType = cameraType;
        this.pictureDate = pictureDate;
        this.cathegory = cathegory;
        this.keyword = keyword;
        this.width = width;
        this.height = height;
        this.maxAmountPublic = maxAmountPublic;
        this.salesPrice = salesPrice;
        this.gps_latitude = gps_latitude;
        this.gps_longitude = gps_longitude;

        this.picByte = picByte;
        this.picSize = picSize;


    }
    public PictureTest(Long id, String title, String description, String cameraType, Date pictureDate, String cathegory, String keyword, int width, int height, int
            maxAmountPublic, int salesPrice, String gps_latitude, String gps_longitude) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.cameraType = cameraType;
        this.pictureDate = pictureDate;
        this.cathegory = cathegory;
        this.keyword = keyword;
        this.width = width;
        this.height = height;
        this.maxAmountPublic = maxAmountPublic;
        this.salesPrice = salesPrice;
        this.gps_latitude = gps_latitude;
        this.gps_longitude = gps_longitude;



    }
    public PictureTest (String title, String type, byte[] picByte) {
        this.title = title;
        this.type = type;
        this.picByte = picByte;
    }


    @Override
    public String toString() {
        return "PictureTest{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", keyword='" + keyword + '\'' +
                ", cameraType='" + cameraType + '\'' +
                ", pictureDate=" + pictureDate +
                ", cathegory='" + cathegory + '\'' +
                ", gps_latitude='" + gps_latitude + '\'' +
                '}';


    }

    public void setPictureCode(String toString) {
    }

    // compress the image bytes before storing it in the database
    public static byte[] compressBytes(byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
        }
        System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);

        return outputStream.toByteArray();
    }










}