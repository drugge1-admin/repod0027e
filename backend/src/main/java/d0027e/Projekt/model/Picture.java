/**package d0027e.Projekt.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Picture implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)

    private int pictureID;

    private int pictureDate;
    private String title;
    private String description;
    private String keyword;
    private String cameraType;
    private int width;
    private int height;
    private int maxAmountPublications;
    private int originalPictureID;
    private int modifVariantID;
    private int fileSize;
    private int salesPrice;
    private int resolution;
    private int gpsCoordinates;
    private String pictureUrl;

    private boolean purchased_selfmade;

    public Picture(){}

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Picture (int pictureID, int pictureDate, String title, String description, String keyword, String cameraType, int width, int height,
                    int maxAmountPublications, int originalPictureID, int modifVariantID, int fileSize, int salesPrice, int resolution,
                    int gpsCoordinates, boolean purchased_selfmade, String pictureUrl) {

        this.pictureID = pictureID;
        this.pictureDate=pictureDate;
        this.title=title;
        this.description=description;
        this.keyword=keyword;
        this.cameraType=cameraType;
        this.width=width;
        this.height=height;
        this.maxAmountPublications=maxAmountPublications;
        this.originalPictureID=originalPictureID;
        this.modifVariantID=modifVariantID;
        this.fileSize=fileSize;
        this.salesPrice=salesPrice;
        this.resolution=resolution;
        this.gpsCoordinates=gpsCoordinates;
        this.purchased_selfmade=purchased_selfmade;
        this.pictureUrl=pictureUrl;
    }

    public int getPictureID() {
        return pictureID;
    }

    public void setPictureID(int pictureID) {
        this.pictureID = pictureID;
    }

    public int getPictureDate() {
        return pictureDate;
    }

    public void setPictureDate(int pictureDate) {
        this.pictureDate = pictureDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getCameraType() {
        return cameraType;
    }

    public void setCameraType(String cameraType) {
        this.cameraType = cameraType;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getMaxAmountPublications() {
        return maxAmountPublications;
    }

    public void setMaxAmountPublications(int maxAmountPublications) {
        this.maxAmountPublications = maxAmountPublications;
    }

    public int getOriginalPictureID() {
        return originalPictureID;
    }

    public void setOriginalPictureID(int originalPictureID) {
        this.originalPictureID = originalPictureID;
    }

    public int getModifVariantID() {
        return modifVariantID;
    }

    public void setModifVariantID(int modifVariantID) {
        this.modifVariantID = modifVariantID;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public int getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(int salesPrice) {
        this.salesPrice = salesPrice;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public int getGpsCoordinates() {
        return gpsCoordinates;
    }

    public void setGpsCoordinates(int gpsCoordinates) {
        this.gpsCoordinates = gpsCoordinates;
    }

    public boolean isPurchased_selfmade() {
        return purchased_selfmade;
    }

    public void setPurchased_selfmade(boolean purchased_selfmade) {
        this.purchased_selfmade = purchased_selfmade;
    }
    @Override
    public String toString(){
        return "Picture{" +
                "pictureID="+pictureID+
                ", pictureDate="+pictureDate+
                ", title='"+title+'\'' +
                ", description='"+description+'\'' +
                ", keyword='"+keyword+'\'' +
                ", cameraType='"+cameraType+'\'' +
                "width="+width+
                "height="+height+
                "maxAmountPublications="+maxAmountPublications+
                "originalPictureID="+originalPictureID+
                "modifVariantID="+modifVariantID+
                "fileSize="+fileSize+
                "salesPrice="+salesPrice+
                "resolution="+resolution+
                "gpsCoordinates="+gpsCoordinates+
                ", purchased_selfmade='"+purchased_selfmade+
                ", pictureUrl='"+pictureUrl+
                '\''+
                '}';

    }
}**/




