package d0027e.Projekt.controller;

import d0027e.Projekt.exception.ResourceNotFoundException;
import d0027e.Projekt.model.Invoices;
import d0027e.Projekt.repo.InvoicesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/invoices")
@Resource
@RestController
public class InvoicesController {

    @Autowired
    private InvoicesRepository invoicesRepository;

    @GetMapping("/invoices")
    public List<Invoices> getInvoices(){
        return invoicesRepository.findAll();
    }

    @PostMapping("/invoices")
    public Invoices createInvoices(@RequestBody Invoices invoices) {
        return invoicesRepository.save(invoices);
    }

    @GetMapping("/invoices/{id}")
    public ResponseEntity<Invoices> getInvoiceById(@PathVariable Long id) {
        Invoices invoices = invoicesRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Invoice not exist with id :" + id));
        return ResponseEntity.ok(invoices);
    }

    @PutMapping("/invoices/{id}")
    public ResponseEntity<Invoices> updateInvoices(@PathVariable Long id, @RequestBody Invoices invoicesDetails){
        Invoices invoices = invoicesRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Invoices not exist with id :" + id));

        invoices.setCompany(invoicesDetails.getCompany());
        invoices.setName(invoicesDetails.getName());
        invoices.setDate((Timestamp) invoicesDetails.getDate());
        invoices.setCost(invoicesDetails.getCost());
        invoices.setState(invoicesDetails.getState());

        Invoices updatedInvoices = invoicesRepository.save(invoices);
        return ResponseEntity.ok(updatedInvoices);
    }

/*    @DeleteMapping("/invoices/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteInvoices(@PathVariable Long id){
        Invoices invoices = invoicesRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Invoice not exist with id :" + id));

        invoicesRepository.delete(invoices);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return ResponseEntity.ok(response);
    }*/
}

    /*@GetMapping ("/find/{id}")
    public ResponseEntity<Customers> getCustomersById(@PathVariable("id")Long id) {
        Customers customers = customersService.findCustomersById(id);
        return new ResponseEntity<>(customers, HttpStatus.OK);
    } */


