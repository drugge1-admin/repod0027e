package d0027e.Projekt.controller;

import d0027e.Projekt.dao.CustomersDAO;
import d0027e.Projekt.dto.CustomersDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import d0027e.Projekt.exception.ResourceNotFoundException;
import d0027e.Projekt.repo.CustomersRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.List;
import d0027e.Projekt.service.CustomersService;

import org.springframework.web.bind.annotation.*;


import d0027e.Projekt.model.Customers;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "/customers")
@RestController
public class CustomersController {

    @Autowired
    public CustomersService customersService;

    private final Logger debugLogger = LoggerFactory.getLogger(this.getClass().getCanonicalName());

    @Autowired
    private CustomersDAO customersDAO;

    @Autowired
    private CustomersRepository customersRepository;

   @GetMapping("/customers")
    public List<Customers> getCustomersByCompany(String company){
        return customersRepository.findAll();
    }

    
    @PostMapping("/customers")
    public Customers createCustomers(@RequestBody Customers customers) {
        return customersRepository.save(customers);
    }

  @GetMapping("/customers/{id}")
    public ResponseEntity<Customers> getCustomersById(@PathVariable Long id) {
        Customers customers = customersRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Customer not exist with id :" + id));
        return ResponseEntity.ok(customers);
    }

    @PutMapping(value="/saveCustomers")
    @ResponseBody
    ResponseEntity<String> saveCustomers(@RequestBody CustomersDTO customers) {
        debugLogger.debug("Entered saveCustomers with id: ", customers.getId());

        try {
            customersDAO.saveCustomers
                    (customers.getId(), customers.getCompany(), customers.getContact(), customers.getEmail(), customers.getDiscount());

            debugLogger.info("Customers with id: ", customers.getId(), " was updated");
            return ResponseEntity.ok("successfully updated customers: " + customers.getId());
        } catch (Exception e) {
            debugLogger.error("Exited saveCustomers with error: ", e);
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }

    @PutMapping("/customers/{id}")
    public ResponseEntity<Customers> updateCustomers(@PathVariable Long id, @RequestBody Customers customersDetails){
        Customers customers = customersRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Customer not exist with id :" + id));

        customers.setCompany(customersDetails.getCompany());
        customers.setContact(customersDetails.getContact());
        customers.setEmail(customersDetails.getEmail());
        customers.setDiscount(customersDetails.getDiscount());

        Customers updatedCustomers = customersRepository.save(customers);
        return ResponseEntity.ok(updatedCustomers);
    }
}


