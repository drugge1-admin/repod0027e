package d0027e.Projekt.dao;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import d0027e.Projekt.model.Customers;

import java.util.List;
import java.util.Optional;

@Transactional
public interface CustomersDAO extends CrudRepository<Customers, Long> {

//    @Query(value = "SELECT * FROM `customers` WHERE Id= :id", nativeQuery = true)

    @Modifying
    @Query(value = "UPDATE customers SET company= :company, contact= :contact, email= :email, discount= :discount, id= :id WHERE id= :id", nativeQuery = true)
    void saveCustomers(@Param("id") Long Id, @Param("company") String company, @Param("contact") String contact, @Param("email") String email, @Param("discount") Long discount);

    @Modifying
    @Query(value = "DELETE FROM customers WHERE id=:id", nativeQuery = true)
    void deleteCustomersById(@Param("id") Long id);

    public Optional<Customers> findById(Long id);

    public List<Customers> findAll();

    public Customers findByEmail(String email);

    @Modifying
    @Query(value="UPDATE customers SET WHERE id= :id", nativeQuery = true)
    void updateCustomers(@Param("id") Long Id);

    @Modifying
    @Query(value = "DELETE FROM `customers` WHERE id = :id", nativeQuery = true)
    void deleteCustomers(@Param("id") Long id);

    public void deleteById(Long id);

//    @Query(value = "SELECT * FROM `customers` WHERE company= :company", nativeQuery = true)
//    Customers[] updateCustomers(@Param("Id") Long Id);

    /*    @Modifying
    @Query(value = "UPDATE `customers` SET company= :company, contact= :contact, email= :email, discount= :discount", nativeQuery = true)
    void saveCustomers (@Param("id") Long id,
                     @Param("company") String company,
                     @Param("contact") String contact,
                     @Param("email") String email,
                     @Param("discount") Long discount
    );*/

/*    List<Customers> findAll();

    public Customers findByEmail(String email);*/

}

