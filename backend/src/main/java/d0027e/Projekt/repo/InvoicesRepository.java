package d0027e.Projekt.repo;

import d0027e.Projekt.model.Customers;
import d0027e.Projekt.model.Invoices;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;


public interface InvoicesRepository extends JpaRepository<Invoices, Long>{

    @NotNull
    Optional<Invoices> findById(Long id);

    List<Invoices> getInvoicesById(Long id);

    void deleteById(@NotNull Long id);
}
