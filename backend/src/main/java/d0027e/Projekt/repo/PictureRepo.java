package d0027e.Projekt.repo;

import d0027e.Projekt.model.PictureTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PictureRepo extends JpaRepository<PictureTest, Long> {

   void deletePictureById(Long id);

    Optional <PictureTest> findPictureById(Long id);

    List<PictureTest> findPictureByTitle(String title);


    Optional<PictureTest> findImageByTitle(String title);

    List<PictureTest> findPictureByCathegory(String cathegory);




}
