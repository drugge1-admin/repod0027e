package d0027e.Projekt.repo;
import java.util.List;
import java.util.Optional;

import d0027e.Projekt.model.Customers;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200")

@EnableJpaRepositories
public interface CustomersRepository extends JpaRepository<Customers, Long>{

    @NotNull
    Optional<Customers> findById(Long id);

    List<Customers> getCustomersByCompany(String company);

    List<Customers> getCustomersById(Long id);

    void deleteById(@NotNull Long id);

}
