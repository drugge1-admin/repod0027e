package d0027e.Projekt.service;

import d0027e.Projekt.model.PictureTest;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileService {

    public PictureTest addPicture(MultipartFile file) throws IOException;


}
