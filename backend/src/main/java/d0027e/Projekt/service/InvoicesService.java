package d0027e.Projekt.service;

import d0027e.Projekt.exception.InvoicesNotFoundException;
import d0027e.Projekt.model.Customers;
import d0027e.Projekt.model.Invoices;
import org.jetbrains.annotations.NotNull;
//import exception.InvoicesNotFoundException;
//import models.Invoices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import d0027e.Projekt.repo.InvoicesRepository;

//import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:4200")
@Service ("invoicesService")

public class InvoicesService {

    private final InvoicesRepository invoicesRepository;

    @Autowired
    public InvoicesService(InvoicesRepository invoicesRepository) {
        this.invoicesRepository = invoicesRepository;
    }
    public List <Invoices> getInvoices() {
        return invoicesRepository.findAll();
    }

    public Invoices createInvoices(@NotNull Invoices invoices ) {
        invoices.setId(Long.parseLong(UUID.randomUUID().toString()));
        return invoicesRepository.save(invoices);
    }

    public List <Invoices> getInvoicesById(Long id) {
        return invoicesRepository.getInvoicesById(id);
    }

    public List<Invoices> findAllInvoices() {
        return invoicesRepository.findAll();
    }

    public Invoices updateInvoices (Invoices invoices) {
        return invoicesRepository.save(invoices);
    }

    public void deleteInvoices(Long id) {
        invoicesRepository.deleteById(id);
    }
}
