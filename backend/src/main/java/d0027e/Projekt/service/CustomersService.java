package d0027e.Projekt.service;

import d0027e.Projekt.model.Customers;
import d0027e.Projekt.model.PictureTest;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import javax.persistence.Id;
import d0027e.Projekt.repo.CustomersRepository;

//import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:4200")

@Service ("customersService")
public class CustomersService {

    private final CustomersRepository customersRepository;

    @Autowired
    public CustomersService(CustomersRepository customersRepository) {
        this.customersRepository = customersRepository;
    }

    public List <Customers> getCustomersByCompany(String company) {
        return customersRepository.getCustomersByCompany(company);
    }

    public List<Customers> findAllCustomers()
    {return customersRepository.findAll();}


    public List <Customers> getCustomersById(Long id) {
        return customersRepository.getCustomersById(id);
    }

    public Customers addCustomers(@NotNull Customers customers){
        customers.setId((long) Integer.parseInt(UUID.randomUUID().toString()));
        return customersRepository.save(customers);
    }

    public void deleteCustomers(Long id) {
        customersRepository.deleteById(id);
    }

}
