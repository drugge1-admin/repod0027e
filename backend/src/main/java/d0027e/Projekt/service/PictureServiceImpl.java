package d0027e.Projekt.service;

import d0027e.Projekt.model.PictureTest;
import d0027e.Projekt.repo.PictureRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;


@Service

public class PictureServiceImpl implements FileService {
  @Autowired
    private PictureRepo pictureRepo;

    private String uploadFolderPath = "Users/irina/Desktop/Picture Database Cod/Pictures to upload";



    @Override
    public PictureTest addPicture(MultipartFile file) throws IOException {
        PictureTest picture=new PictureTest();
        picture.setPicByte(file.getBytes());
        picture.setType(file.getContentType());
        picture.setTitle(file.getOriginalFilename());
        PictureTest pictureToRet= pictureRepo.save(picture);
        return pictureToRet;
    }

    public List<PictureTest> findAllPictures()
    {return pictureRepo.findAll();}

    public PictureTest updatePicture(PictureTest picture)
    {return pictureRepo.save(picture);}


    /**public Optional<PictureTest> findPictureById(Long id){
     return pictureRepo.findPictureById(id);}**/



   /**public List <PictureTest> findPictureByTitle(String title){
        return pictureRepo.findPictureByTitle(title);}**/

    public void deletePicture(Long id){
        pictureRepo.deletePictureById(id);
    }




    }




