package d0027e.Projekt.exception;

public class InvoicesNotFoundException extends RuntimeException {
    public InvoicesNotFoundException(String message) {
        super(message);
    }
}
