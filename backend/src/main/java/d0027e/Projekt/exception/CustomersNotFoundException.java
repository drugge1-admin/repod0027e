package d0027e.Projekt.exception;

public class CustomersNotFoundException extends RuntimeException {
    public CustomersNotFoundException(String message) {
        super(message);
    }
}
