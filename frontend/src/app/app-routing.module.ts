import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from "./app.component";
import { SearchPicComponent} from "./components/search-pic/search-pic.component";
import {StartpageComponent} from "./components/startpage/startpage.component";
import {AboutComponent} from "./components/about/about.component";
import {CustomersComponent} from "./components/customers/customers.component";
import {UploadPicturesComponent} from "./components/upload-pictures/upload-pictures.component";
//import {UploadPicturesControlComponent} from "./components/upload-pictures-control/upload-pictures-control.component";
//import {UploadSuccessComponent} from "./components/upload-success/upload-success.component";
import {InvoicesComponent} from "./components/invoices/invoices.component";
import {LogoutComponent} from "./components/logout/logout.component";
//import {UploadPicturesAllComponent} from "./components/upload-pictures-all/upload-pictures-all.component";
//import {CustomersaddDialogComponent} from "./dialogscustomers/add/customersadd.dialog";
//import {CustomerseditDialogComponent} from "./dialogscustomers/edit/customersedit.dialog";
//import {CustomersdeleteDialogComponent} from "./dialogscustomers/delete/customersdelete.dialog";
import {InvoicesaddDialogComponent} from "./dialogsinvoices/add/invoicesadd.dialog";
//import {UploadPicturesAllComponent} from "./components/upload-pictures-all/upload-pictures-all.component";
// import {InvoicesaddDialogComponent} from "./dialogsinvoices/add/invoicesadd.dialog.component";
// import {InvoiceseditDialogComponent} from "./dialogsinvoices/edit/invoicesedit.dialog.component";
import {ModifyCustomersComponent} from "./components/dialogs/modify-customers-component/modify-customers-component";



const routes: Routes = [
  { path: '', component: StartpageComponent},
  { path: 'search-pic', component: SearchPicComponent },
  { path: 'startpage', component: StartpageComponent},
  { path: 'about', component: AboutComponent},
 // { path: 'upload-pictures-all', component: UploadPicturesAllComponent},
  { path: 'customers', component: CustomersComponent},
  { path: 'upload-pictures', component: UploadPicturesComponent},
 // { path: 'upload-pictures-control', component: UploadPicturesControlComponent},
 // { path: 'upload-success', component: UploadSuccessComponent},
  { path: 'invoices', component: InvoicesComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'invoicesadd', component: InvoicesaddDialogComponent},
  // { path: 'invoicesedit', component: InvoiceseditDialogComponent},
  { path: 'modify-customers-component', component: ModifyCustomersComponent},
  { path: '**', redirectTo: '' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
