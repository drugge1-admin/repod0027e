import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//material imports
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from '@angular/material/button';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { RegisterComponent } from './components/register/register.component';
import { StartpageComponent } from './components/startpage/startpage.component';
import { AppComponent } from './app.component';
import { ViewComponent } from './components/view/view.component';
import {MatToolbar, MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatListModule} from "@angular/material/list";
import { SearchPicComponent } from './components/search-pic/search-pic.component';
import { MatFormFieldModule } from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { RouterModule} from "@angular/router";
import { MatInputModule } from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {MatDatepickerModule} from "@angular/material/datepicker";
import { MatNativeDateModule} from "@angular/material/core";
import { AboutComponent } from './components/about/about.component';
import {UploadPicturesComponent} from "./components/upload-pictures/upload-pictures.component";
//import { UploadPicturesControlComponent } from './components/upload-pictures-control/upload-pictures-control.component';
//import { UploadSuccessComponent } from './components/upload-success/upload-success.component';
import { CustomersComponent } from './components/customers/customers.component';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSortModule} from "@angular/material/sort";
import { InvoicesComponent } from './components/invoices/invoices.component';
//import {UploadPicturesAllComponent} from "./components/upload-pictures-all/upload-pictures-all.component";
import {HttpClientModule} from "@angular/common/http";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
//import {DataService} from './services/data.service';
//import {AddDialogComponent} from './dialogs/add/add.dialog.component';
//import {EditDialogComponent} from './dialogs/edit/edit.dialog.component';
//import {DeleteDialogComponent} from './dialogs/delete/delete.dialog.component';
import {MatDialog, MatDialogModule} from "@angular/material/dialog";
import {DataSource} from "@angular/cdk/collections";
import {AjaxResponse} from "rxjs/ajax";
//import {CustomersComponent} from "./components/customers/customers.component";
//import {CustomersaddDialogComponent} from './dialogscustomers/add/customersadd.dialog';
//import { CustomersdialogComponent } from './components/customersdialog/customersdialog.component';
import {CustomersService} from "./services/customers.service";
import {ModifyCustomersComponent} from "./components/dialogs/modify-customers-component/modify-customers-component";
//import { InvoicesdialogComponent } from './components/invoicesdialog/invoicesdialog.component';
import{PictureService} from './pictureService';
import {InvoicesaddDialogComponent} from "./dialogsinvoices/add/invoicesadd.dialog";
// import {InvoiceseditDialogComponent} from "./dialogsinvoices/edit/invoicesedit.dialog.component";
// import {InvoicesdeleteDialogComponent} from "./dialogsinvoices/delete/invoicesdelete.dialog.component";
import {PopUpComponent} from "./components/pop-up/pop-up.component";
import { AlertComponent } from './components/alert/alert.component';
import { InvoiceseditComponent } from './dialogsinvoices/edit/invoicesedit.component';
import { EditCustomersComponent } from './components/dialogs/edit-customers-component/edit-customers-component';
import {MatTooltipModule} from "@angular/material/tooltip";

@NgModule({
  declarations: [

    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    StartpageComponent,
    ViewComponent,
    SearchPicComponent,
    AboutComponent,
    UploadPicturesComponent,
    //UploadPicturesControlComponent,
   // UploadSuccessComponent,
   // UploadPicturesAllComponent,
    CustomersComponent,
    InvoicesComponent,
    ModifyCustomersComponent,
    InvoicesaddDialogComponent,
    // InvoiceseditDialogComponent,
    // InvoicesdeleteDialogComponent,
    PopUpComponent,
    AlertComponent,
    InvoiceseditComponent,
    EditCustomersComponent,

   // CustomersdialogComponent,
   // InvoicesdialogComponent,
    //AddDialogComponent,
    //EditDialogComponent,
    //DeleteDialogComponent,


  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatButtonModule,
        MatToolbarModule,
        MatIconModule,
        MatSidenavModule,
        MatListModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        RouterModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatDialogModule,
        HttpClientModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
        MatDialogModule,
      MatTooltipModule,



    ],
  providers: [PictureService, CustomersComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
