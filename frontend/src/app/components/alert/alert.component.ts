import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';

import { Alert, AlertType } from 'src/assets/models/alert.model';
import { AlertService } from '../../services/alert-service';

@Component({ selector: 'app-alert', templateUrl: 'alert.component.html' })
export class AlertComponent implements OnInit, OnDestroy {

  @Input() id: string | undefined;

  alerts: Alert[] = [];
  subscription: Subscription | undefined;

  constructor(private alertService: AlertService) { }

  ngOnInit() {
    this.subscription = this.alertService.onAlert(this.id)
      .subscribe(alert => {
        if (!alert.message) {
          this.alerts = [];
          return;
        }
        this.alerts.push(alert);
      });
  }

  ngOnDestroy() {
    if (!this.subscription) { return; }
    this.subscription.unsubscribe();  // unsubscribe to avoid memory leaks
  }

  removeAlert(alert: Alert) {
    this.alerts = this.alerts.filter(x => x !== alert);
  }

  cssClass(alert: Alert) {
    if (!alert) { return; }

    switch (alert.type) {
      case AlertType.Success:
        return 'alert alert-success';
      case AlertType.Error:
        return 'alert alert-danger';
      case AlertType.Info:
        return 'alert alert-info';
      case AlertType.Warning:
        return 'alert alert-warning';
      default:
        return;
    }
  }

}
