import { Component, OnInit, Inject} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MAT_DIALOG_DATA } from "@angular/material/dialog";
import{PictureService} from "src/app/pictureService";
import{PictureTest} from "src/app/pictureTest";
import {FormsModule, ReactiveFormsModule, NgForm} from "@angular/forms";
import {HttpErrorResponse} from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DOCUMENT, DatePipe } from '@angular/common';
import { VERSION, ViewChild, ElementRef } from "@angular/core";
import { HttpClient, HttpEventType } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';





@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.css']
})
  export class PopUpComponent implements OnInit {
  name = "Angular " + VERSION.major;

  public pictures!: PictureTest[];
  userFile ;


  myPictures: any;
  myTitle="";
  myMessage="";
  imgURL: any;
  public imagePath;


  picture={

  title: "",
  description: "",
  cameraType: "",
  cathegory: "",
  keyword: "",
  pictureDate: Date.now(),
  width: "",
  height: "",
  maxAmountPublic: "",
  salesPrice:"",
  gps_latitude:"",
  gps_longitude:"",
  type:"",
  picByte:"",
  picSize:"",
  };
  //public dataForm:  FormGroup;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public pictureService:PictureService, public fb: FormBuilder,
    private httpClient: HttpClient,
    private router : Router
    ) {
    }

    infoForm() {
    this.pictureService.dataForm = this.fb.group({


            'id': null,
            'pictureDate': Date.now(),
            'title': [''],
            'description': [''],
            'cameraType': [''],
             'cathegory': [''],
            'keyword': [''],
            'width': [''],
            'height': [''],
            'maxAmountPublic': [''],
            'salesPrice': [''],
            'gps_latitude': [''],
            'gps_longitude': [''],
            'type': [''],


          });
        }

   /**public getPictures(): void {
       this.pictureService.getPictures().subscribe(
       (response: PictureTest[])=>{
       this.pictures=response;

       },
       (error: HttpErrorResponse)=>{
       alert(error.message);
       }
     );
     }**/

        public savePicture(): void{
        const data={

        title: this.picture.title,
        description: this.picture.description,
        cameraType: this.picture.cameraType,
        cathegory: this.picture.cathegory,
        keyword: this.picture.keyword,
        pictureDate: this.picture.pictureDate,
        width: this.picture.width,
        height: this.picture.height,
        maxAmountPublic: this.picture.maxAmountPublic,
        salesPrice: this.picture.salesPrice,
        gps_latitude: this.picture.gps_latitude,
        gps_longitude: this.picture.gps_longitude,
        type:this.picture.type,
        picByte: this.picture.picByte,
        picSize: this.picture.picSize,

};

        /**this.pictureService.addPicture(data)
        .subscribe(
        response=>{
        console.log(response);
        this.myMessage="Bilden är uppladdad";

        },
        error=>{
          console.log(error);}
      );**/

}

    get f() { return this.pictureService.dataForm.controls; }

  ngOnInit(): void {
      //this.getPictures();
      this.myMessage="";
      this.infoForm();


    }
  url="./assets/images/myra.png";

 /** onselectFile(e){
    if(e.target.files){
      var reader=new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload=(e: any)=>{
        this.url=e.target.result;
      }
    }
}**/


selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResponse: any;
  message: string;
  imageName: any;



  //Gets called when the user selects an image
  public onFileChanged(event) {
    //Select File
    this.selectedFile = event.target.files[0];
  }


  //Gets called when the user clicks on submit to upload the image
  onUpload() {
    //console.log(this.selectedFile);
    this.savePicture();
    //FormData API provides methods and properties to allow us easily prepare form data to be sent with POST HTTP requests.
    const uploadImageData = new FormData();
    uploadImageData.append('file', this.selectedFile, this.selectedFile.name);


    //Make a call to the Spring Boot Application to save the image
    this.httpClient.post('http://localhost:8080/picture/add', uploadImageData, { observe: 'response' })
      .subscribe((response) => {
        if (response.status === 200) {
          this.message = 'Image uploaded successfully';
        } else {
          this.message = 'Image not uploaded successfully';
        }
      }
      );


  }



    //Gets called when the user clicks on retieve image button to get the image from back end
    getImage() {
    //Make a call to Sprinf Boot to get the Image Bytes.
    this.httpClient.get('http://localhost:8080/picture/find_1/' + this.imageName)
      .subscribe(
        res => {
          this.retrieveResponse = res;
          this.base64Data = this.retrieveResponse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        });
}
  onSubmit(){


          this.addData();
}

    onSelectFile(event) {
      if (event.target.files.length > 0)
      {
        const file = event.target.files[0];
        this.userFile = file;
        //this.f['picByte'].setValue(file);

      var mimeType = event.target.files[0].type;
      if (mimeType.match(/image\/*/) == null) {
        this.message = "Only images are supported.";
        return;
      }

      var reader = new FileReader();

      this.imagePath = file;
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        this.imgURL = reader.result;
      }
    }


      }



  addData(){
      const formData = new FormData();
      const picture = this.pictureService.dataForm.value;
      formData.append('picture',JSON.stringify(picture));
      formData.append('file',this.userFile);
      this.pictureService.createData(formData).subscribe( data => {
      this.message="Bilden är uppladdad";
        //this.router.navigate(['/add']);
      });}

}





