/**import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import{PictureTest} from "src/app/pictureTest";
import{PictureService} from "src/app/pictureServiceImpl";
import {HttpErrorResponse} from '@angular/common/http';
import { Inject }  from '@angular/core';
import { VERSION, ViewChild, ElementRef } from "@angular/core";

@Component({
  selector: 'app-upload-pictures',
  templateUrl: './upload-pictures.component.html',
  styleUrls: ['./upload-pictures.component.css']
})
export class UploadPicturesComponent implements OnInit {**/




//constructor(private pictureServiceImpl: PictureService) { }






 /** public onOpenModal(picture:PictureTest|null, mode: string): void{
  const div=document.getElementById('main-container');

  const button =document.createElement('button');
  button.type='button';
  button.style.display='none';
  button.setAttribute('data-bs-toggle', 'modal');
    if (mode==='add'){
       button.setAttribute('data-bs-target', '#addPictureModal');
        }


   div.appendChild(button);
   button.click();
}**/

//angular
import { Component, ViewChild, ElementRef, OnInit } from "@angular/core";
import { DOCUMENT} from "@angular/common";
import{PictureService} from "src/app/pictureService";
import{PictureTest} from "src/app/pictureTest";
import {MatDialog} from '@angular/material/dialog';
import { PopUpComponent } from 'src/app/components/pop-up/pop-up.component';
import {HttpErrorResponse} from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Inject }  from '@angular/core';

@Component({
  selector: 'app-upload-pictures',
  templateUrl: './upload-pictures.component.html',
  styleUrls: ['./upload-pictures.component.css']
})

export class  UploadPicturesComponent implements OnInit{

      public pictures!: PictureTest[];



       constructor(
          private dialogRef:MatDialog,
          private pictureServiceImpl: PictureService ) {}

          openDialog(){
          this.dialogRef.open(PopUpComponent,{
          //data :{ name: "Misha"}
        });
        }



public getPictures(): void {
       this.pictureServiceImpl.getPictures().subscribe(
       (response: PictureTest[])=>{
       this.pictures=response;

       },
       (error: HttpErrorResponse)=>{
       alert(error.message);
       }
     );
     }
     ngOnInit(){
           this.getPictures()}

}












