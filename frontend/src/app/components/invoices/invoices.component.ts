import {AfterViewInit, OnInit, Component, ViewChild, ElementRef} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {Data, Router} from "@angular/router";
import {InvoicesService} from "../../services/invoices.service";
import {Invoicesmodel} from 'src/assets/models/Invoicesmodel';
import {BehaviorSubject, fromEvent, map, merge, Observable, tap} from "rxjs";
import {InvoicesaddDialogComponent} from "../../dialogsinvoices/add/invoicesadd.dialog";
import {InvoiceseditComponent} from "../../dialogsinvoices/edit/invoicesedit.component";

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})

export class InvoicesComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = [
    'id',
    'company',
    'name',
    'date',
    'cost',
    'state',
    'editInvoices',
    'deleteInvoices'];

  dataSource: any = new MatTableDataSource<Invoicesmodel>();

  invoicesmodel: Invoicesmodel[] = [];
  isTableLoaded: boolean = false;
  showAll: boolean = false;

  public id: number;
  private index: any;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Invoicesmodel>;

  constructor(private invoicesService: InvoicesService,
              private router: Router,
              private dialog: MatDialog,
              private httpClient: HttpClient,
              private dialogRef: MatDialog) {
  }

  openDialogAdd(){
    this.dialogRef.open(InvoicesaddDialogComponent,{
    });
    this.table.renderRows();
  }

  setTableIsLoaded() {
    this.isTableLoaded = true;
  }

  //Opens dialog to edit invoices, in the addinvoicescomponent
  editInvoices(invoicesmodel  : Invoicesmodel) {
    const dialogRef = this.dialog.open(InvoiceseditComponent, {data: invoicesmodel});
  }

  ngOnInit(): void {
    this.getInvoicesToTable();
    this.dataSource = new MatTableDataSource<Invoicesmodel>();
    this.dataSource = new MatTableDataSource(this.invoicesmodel);// create new object
    console.log(this.dataSource);
  }

  //Methods that remove 1 table row and deletes row in database
  deleteInvoices(id: number, element : Invoicesmodel) {
    this.invoicesService.deleteInvoices(element.id).subscribe(
      data => {
        console.log('deleted response', data);
      })
    this.removeTableRow(element);
    console.log(element);
  }
  removeTableRow (element: Invoicesmodel) {
    let arr: Invoicesmodel[] = this.dataSource.data;
    arr.splice(arr.indexOf(element), 1);
    this.dataSource.data = arr;
  }

  public getInvoicesById(id: number){
    this.invoicesService.getInvoicesById(id).subscribe(data => {
      this.invoicesmodel = data;
    });
  }

  createInvoices(){
    this.router.navigate(['create-invoices']);
  }

  applyFilter(event: Event) {

    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getInvoicesToTable() {
      this.invoicesService.getInvoicesToTable().subscribe(invoices => {
        console.log(invoices);
        this.dataSource.data = invoices;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}

