import {AfterViewInit, OnInit, Component, ViewChild, ElementRef, SimpleChanges, ChangeDetectorRef} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {Customersmodel} from 'src/assets/models/Customersmodel';
import {Data, Router} from "@angular/router";
import {CustomersService} from "../../services/customers.service";
import {ModifyCustomersComponent} from "../dialogs/modify-customers-component/modify-customers-component";
import {CustomersRequest} from "../../../assets/models/CustomersRequest";
import {EditCustomersComponent} from "../dialogs/edit-customers-component/edit-customers-component";
import {InvoicesaddDialogComponent} from "../../dialogsinvoices/add/invoicesadd.dialog";


@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})

export class CustomersComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = [
    'id',
    'company',
    'contact',
    'email',
    'discount',
    'editCustomers',
    'deleteCustomers'];

  dataSource: any = new MatTableDataSource<Customersmodel>();

  customersmodel: Customersmodel[] = [];

  showAll: boolean = false;

  isTableLoaded: boolean = false;

  private company: string;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Customersmodel>;

  public id: number;

  constructor(private customersService: CustomersService,
              private router: Router,
              private httpClient: HttpClient,
              private dialog: MatDialog,
              private changeDetectorRefs: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<Customersmodel>();
    this.dataSource = new MatTableDataSource(this.customersmodel);// create new object
    this.getCustomersToTable();
    console.log(this.dataSource);
  }

  //Loads customers from database to table through service
  getCustomersToTable() {
    this.customersService.getCustomersToTable().subscribe(customers => {
      console.log(customers);
      this.dataSource.data = customers;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  //Methods that remove 1 table row and deletes row in database
  deleteCustomers(id: number, element : Customersmodel) {
    this.customersService.deleteCustomers(element.id).subscribe(
      data => {
        console.log('deleted response', data);
      })
    this.removeTableRow(element);
    console.log(element);
  }
  removeTableRow (element: Customersmodel) {
    let arr: Customersmodel[] = this.dataSource.data;
    arr.splice(arr.indexOf(element), 1);
    this.dataSource.data = arr;
    this.table.renderRows();
  }

  //Adds customers with the modifycustomerscomponent
  addCustomers() {
    const dialogRef = this.dialog.open(ModifyCustomersComponent, {data: this.customersmodel});
    this.table.renderRows();
  }

  private updateTableData(data: any[]) {
    this.dataSource = data && data.length ? new MatTableDataSource(data) : new MatTableDataSource([]);
  }

  //Opens dialog to edit customer, in the modifycustomerscomponent
  editCustomers(customersmodel: Customersmodel) {
    const dialogRef = this.dialog.open(EditCustomersComponent, {data: customersmodel});
  }

  openDialogAdd(){
     this.dialog.open(ModifyCustomersComponent,{
     });
    this.table.renderRows();
  }

  setTableIsLoaded() {
    this.isTableLoaded = true;
  }

  initTable() {
     this.customersService.getCustomersList(this.CreateCustomersRequest()).subscribe(data=>{
      if (data.listOfAllCustomers == null) {
        this.setTableIsLoaded();
      } else {
        let tableList: Customersmodel[] = [];
        let lst: Customersmodel[] = data.listOfAllCustomers;

        if (lst.length == 0) {
          this.setTableIsLoaded();
        }

        let i = 0;
        while (i < lst.length) {

          tableList.push({
            id: lst[i].id,
            company: lst[i].company,
            contact: lst[i].contact,
            email: lst[i].email,
            discount: lst[i].discount,

          });
          i++;
        }
        this.dataSource.paginator = this.paginator;
        this.paginator.pageSize = 20;

        this.dataSource.data = tableList;
        this.isTableLoaded = true;
      }
    });
  }

  CreateCustomersRequest() : CustomersRequest {
    let customersRequest = {
      id: null,
      company: "",
      contact: "",
      email: "",
      discount: null,
      all: this.showAll,
    };
    return customersRequest;
  }

  translateUserTableToCustomers(customers: Customersmodel) : Customersmodel {
    return {
      id: (customers.id == null ? 0 : customers.id),
      company: customers.company,
      contact: customers.contact,
      email: customers.email,
      discount: customers.discount,
    }
  }

  requestDelete(customers: Customersmodel) {
    if (customers.id == null) {
      this.initTable();
      return;
    }
  }

  updateCustomersInTable(customers: Customersmodel) {
    let newArr: Customersmodel[] = [];
    let oldArr = this.dataSource.data;
    this.dataSource.data = [];
    for (let u of oldArr) {
      if (u.id == customers.id) {
        u = customers;
      }
      newArr.push(u);
    }
    this.dataSource.data = newArr;
  }

  addCustomersInTable(customers: Customersmodel) {
    let newArr: Customersmodel[] = [];
    let oldArr = this.dataSource.data;
    this.dataSource.data = [];  //Required for table to actually show correct information after for loop.

    for (let u of oldArr) {
      newArr.push(u);
    }

    newArr.push(customers);
    this.dataSource.data = newArr;
  }

  getNewTableContent() : Customersmodel {
    return {
      id: 0,
      company: "",
      contact: "",
      email: "",
      discount: null
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

ngAfterViewInit() {
  this.dataSource.paginator = this.paginator;
}

}
