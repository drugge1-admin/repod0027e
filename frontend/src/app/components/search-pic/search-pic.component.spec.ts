import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPicComponent } from './search-pic.component';

describe('SearchPicComponent', () => {
  let component: SearchPicComponent;
  let fixture: ComponentFixture<SearchPicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchPicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
