import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import{PictureService} from "src/app/pictureService";
import{PictureTest} from "src/app/pictureTest";
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {FormsModule, ReactiveFormsModule, NgForm} from "@angular/forms";
import {MatDialog} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpEventType } from '@angular/common/http';




interface Choice {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-search-pic',
  templateUrl: './search-pic.component.html',
  styleUrls: ['./search-pic.component.css']
})

export class SearchPicComponent implements OnInit {
  dateFrom: FormGroup;
  dateTo: FormGroup;
  value = 'clear';
  //ny kod
  pictures: PictureTest[];
  currentPicture:PictureTest;
  currentIndex=-1;
  title: "";
  cathegory: "";
  // base64Data1: any;
  //base64Data1="iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAkFBMVEX///+pqakAAAAfGhetra3V1dWurq4XEQ0QBwAMAAAdGBUNAAB0c3FxcG4IAAAVDgkRCQE2MjA8ODZCPz0wLCqenZxta2ksKCVRTkxeXFpHREKlpKR5d3bu7u0oIyGFhIO+vbzm5eVMSUeTkpJhX17Ew8Pb29qOjYyFg4JXVVOXlpXNzMv29va3trbBwMBgXltIq2GZAAAVVklEQVR4nO1daXejuBINJQUJGZDYwWC8YWNsnPn//+5J4H3JJN2TZ9Gn7zlzOnH8QXdUqrpVkkpvb3/xF3/xB2Fn7NevHsOPYgUS1J61rx7IjwEDQoiMgBW7Vw/lh9AGlAWNT7gFQTR59Wh+BBMfmBc7acI5kGL+6uH8CAogJLWLOEDEgub91cP5CcxCIElm23GaEAHNn7ggJxsQxK9s22nCP5XjOgXO/FhyzEIuoPoTQ+SHB5wojnam5tEeoF+d58u6XkbGx3ryOLybHUdpq3ZTSp9j/J/H99uQ4sV1oUeZFtuP+R3P1vAlRy9zbCctCaQDc6tLKV6SwPcSRDgXluTpZfXqJv61+0ByTNJYxg75peg1Q/1FTGRIQGGqnEnVpH4peVIXiD9dXX/vYwEWQf4/RSWnsRrWasxDsDhHQRM7dlHYcROo+ZSWG1+TXBcMBGfjzGcUBiZydoUvEwnOS3+RxbGk6fyzCBGh4BdXkzXBjQuUSE0O+1eN9VfRrqO4lD6HE1J6/qKp4rhKPUTAvvniOpIRUjKcvWKUv4t2jTeBcqnSZglDZVkixMr7700qwsjArPQS61kUB2zUBRD+kGEbMuYNPjNu5++71crhjN17zRUg3rxgTD+BqcXEvRItKHLrF4zmJ6C43AsYxhCsHnx7iHAEgruMaSdF0NDC4VNU/AHDzQix8BWj+QlkjxjKQEI3rxjNT6Ah9ytuDUON94+QSoYfN5/liuGwhPcnUAxnN581HBH/FYP5ETxg2CKG3OVLRvMTODOczCWUbXax4s+puSlPs3vbRdU4IaMRSRbObGpJrTp4UXqCWnNpopINQoiwLNdVmZNwXj2u38N8tZ+tD5MkIz7iAnmLhcwYi3pajEsmjdR87Qh/C/Ol1xXdRoWUo+2yZJyM7XiBCBeUksXUrIXKGWevHucvI6KSHfISRbLACQhURbXPLRBBGlCwrECuTEQI5K8e6a+hrQDYdt227WQ/BgsIiaN9zAXEq7Z9a9tVDFwS5HHAh1cW7pACbE5eci8Ir02zct3xOUNc+6400sgY80GmTzZczcw64UnuWDelqBhYYBqGx8vhCbcVwHUpe22RALnxzddii20xjggU/7+h/UcI3ermEwMYuUsE25CPsYFtC4a20TYD627IAYH7GsYO6BRLO3VvK6m6I4PbKZTeBm5tVCGlahILAcPSbpPyLhGUFkkfSewV8Mgw8nJg0uYd3JNzPM/Nw82Jlo8KbJiplf0fxvXfYQ+o/2G+8RPfvlqS75t0kV2cGqrcSpmpNawUI4I+b58pwUboReBoY7Vhw+Gs1GoIJMMlcQflTbcwVv/sIGrbrUwBT8G/9aGMK0bkR8cCWw6hdKbGo5WrMXLw1D+8S/1ijk77FQ14c2moXdZ0mEUTSsnQ9GBQm93SQb4pG92qX2pL0ul+kp+4HY9CZU2HEpvRzaE5hkFVTdeu0ihFb6uByiB6TzkmfdnJ4Wrjt5+0LfiKYTqwmO+r4S+I8PM8UWzUDuEkn1PGmNQ1M8HUcdPeG9luphg2d5pVb0yVM10QRGSOiA4Ml+MVKONcBIfPrO6rycgeIkOZWuzVXtMRvHmbuLym6mdC+s9YpwreYbQdopVmnJTtHk4MYfkWU5YwdAFmqRjvWL6MFYPzNDNgjKZqX+lAhk+2QNgVQcSU7pkDLVQ4HFq0CMR4St3GPNgjonkEPPWuGQpllo0IczWHMuLPXj3qb2AP7ta0hfAXPSfSZEAX5oZeMVTV/CXQjZpCY8seJI/6IhCpaeAN4adJ425mGLgRFwRppvQrzUxFENf0wTENbbECayknBkcL4EyBuOG0m6mAn72rzCVsEEH3uYEdy3v1sL+BLm3vHGQUB0lZelltHoiM6WFaWbA2XJem/efSlQ4pWMxBbA4DN7Bpqv+Ovxo4ZgfTXQgQzDn+IU+GdHhvCwf3+Ah4m6GR2nuiFsoi8/hpTYe01e3RynzK0MA4n8ZN2sTT/DSzhlnRAS3Dd+BL/JxhR7LD5UfhkOL9FpLPCT6gXFtD2glurOwTI30IMyWM3hdYNcUEWdPvzmE0Yj4D/OqhfxHvAM896ZMpjDlyfDKU/acIyu8aaU7I2HYY8dVusYwhmgvUAtJvMlRTGFe+TLEotSh1AZJa5+OYjRt/cxlGiARjQqykcTb11M58SsHdaMux9aziewzNiqOS0HCT92HSNLYxGrlM1wDZcqv+FkO8lPkGE/GlAsCGzSgs9PQ8E5n8fo9hQBBj09PaNVe71d7AecMp1/LAm2QYfYegWYxkMrw5Edy/d9gb5lSmzbNX03mA+fcY4kim/eJCqe96hu8f8k8J1zGl+uYcYl/aaHLx+/v7mWLu6XjQRjL8l8ziEmYllI1epFEnhu9qMfpcvzMa7XdkKS4EL1l4+cmZ4buy4ZCHunnUNnDtrzLESzaqfHGVL+/ODGeqIsD1286I3a8mTzhCPJwDvYqfq4tJzJWrdbXzNksIv8YQ5wnn8x2UV57pYiG+d9X+lHPN7HT3xexJeUpr95Z3+6Nn5B+XvsZQqlW3OuMk+ZIwxZH0k7O3tw0sbqZ8d80Q20K3i1+O6/+7mSov2W3FxG5z8218pLg+/I/yLM2czQ74v2pvc8l4f7oke+CYDhR3vbXjglqaTeL4uBnxfAYLS7BeVmfuP3cM873iuDv9XsL0xZRusAPx6UqUeYPl+odpubfSjqO5P39qxpZuN6ErF32SQZl1KKA6Drm48zT3iEA3eTopeZI/oWjmGRcX1w8iCD7bAlit5F9N39Vtj//d5WX0iCI2HCYguFDTM0g+2cY5KHDH8jUz07cV5aTANxwxzm1EgV3dH1kDeW7RHwcFvnV1C4nS25QuDWrjtHGIsZnXjeQnNjca7HAW4yF2RwWuY2uQSQyC+nEdYdM0cbQs0pBSgOndXDTWI2fa4xAXDTwGHW9h7sYAMuyVoReWiI8sAN94sJry5wsRHxmamX45VIf11CeHllE8yYzHS2kO7lMJdJrD2Ar+z2P/MuZyFeXGbPeJo/jETHdHhoX1oCnKYGCCeGams4OnwRuLv3qYv4G2pPGnkygX5GYkXj3M38ESyNOgv3vfmUqpj9CrR/k7mCDx9PBGbijyStS8epS/hS2Qz2usZnV3E25gSHjwGUEV8QfehWgH1Pk0h2IaqrbvYfNJ2FfHbdwhnQl7iHYs2MN0q1+GzWj4zcDmiIenpBnfBI98dEqZ2ydNUQeAHSfegSJelv61J+VcCu/1vkj9MAn91DE0P4/yGA0hSW+oOOPuxf5Vt9EPta9av7rWyHJVB1i0GR7JjCNG+u38gPHT0VppsiErS+RSkYzjzXQ63cSLkFAXxrNXD/mbiNWRDKtSlloycko3cK4uhjGOmjo38fk8ii8ojDWrwP0L1BUi5vKwwAZBZHxgaG491RyEV5F56WoxNurUEhBrV775BBuK3CiCkQhtgphn9rWdSl27of6DSILxMuB0NCAlsHQRGG/zGFyqetZEeVQXi5G6vUAr82GoxGbAhtRuwoC+oLYu1BVa1vlNgkq5BIsngs6sKPEFpLrLnXa+3s1wnhenloP9LUXVJ5uj5BOCjkUzM6aWp+1inOz2dTUOWd/gXJxaYO5Axo3JfNK2Y4Ke5v9mQflC/rPhotSQYrvLq9KSMZwKTlST4SQsTww/JEPa/bQH8mzfRhHsLy7WTDeKk49aNVCmXK6yMEizKnYKpxrLpbfovzBTDDv5WQn0pL6BY0sS7ByQuWTC02ctrqPMVU2wkZSX/8SOLeHEja+afZfxQYgphiPFcAWjzWMvirORCI5K3ay5lb6U1Qmt2YBqw+4tsp6c6kzvJ0zSg+ZcD98rhmpSfDF+TDAKKE3P+3fmhrhaUFwlqrlXkPXk4qoJQqlWVMezyry0siPDHYiHabE5LYnqFnJmvCRIh7sMayDMy2LJLF3IiePdkwI0qfLbLGF/sNLTJb/rCcxTl5DNhQPCEWGEabD/NgMmo4AkxoU6jw8sqOqPRycPD+twbYn7M4DYKBCnwaWKw1FCyjqhGtjptGu+B1Y5rorlfn3/tMcBH70v3UL5gJ9HObONS4K5T0ht1q4Onfta1bZUBvJ/+ZrUNEy8vS2s6zqx1OF2KLjbXMlwdQZV5ctmI3Q7//YcStPwt7l7WX6TWWHdEMHpYntNOwq52y3KHLmDaRP6rlTbGwZuHriZeVQ0zOIUNdvrNANvpTfuvQ4u6GDuu89BvaFQuOnexHm0ncaNJ6gQPLCjmzTKnEonenJHPh1KzXECqrHLmHuLwA+l5xTS95KFExm3nhXHMr6edjzwVAxmEiXD5s1SF71GqrSWpHad3x5dUT5mLER4djs4JULv/Zt2vtvnyyKuKoaoPQfSZLG96Q5z3NOTy3MqJUNz/osZjwjTtV/YZI3r2O+PNLgW5Qi5WxkysHl7H/piBUapxdFFcmwWrshCV7NDjG/dYzRFyhQxmSfKbMoPxgtPtZAywH/MrTfQmHF3HJ0J4pryADtWolfRf51XoTpzwznyxqmUrI5dyFSqlFn/ZAuP8wo1XYadCIEuT5XhJSNhjnNLpxYw75uyT4NVLmX3udQ/i0QlG6jevU2heZIa5nEp/59U+VVygQiSPgcvtDlZ1BqJyoNLv6l6cnHcjEPChQtl1VWyC3hwbUPO2jLjgpMmuvyjItid/8MbS5Mmoe+hyqQWVfdqWVylY6/s2LmLze4gLp07hn0xfySVTXat3MyaHcNizvQw09YXqFw0TToOvLBkhAjV1bypPy6k8xVDKdzMaJp5XAjXc6JrByszfFIe4r40Uy0qxK0vp5CcEkWgTX33pLWy0m4XRiKqiyZkUtu4rFreKhszFrw8HlE1K67H0aLJJggTtdcZT42P9cOVM4XxtCicOBsnIxkjhXSeflybt/V9nC8s4R+9Dq75cN4dWrrMUlCqlEp/G9fRA2ljLhMpbE50I8SGc2pj66Iy8fyxlG6bZWQ8Em7SXqX0JqfLgDgPSeJpd+j9CXLCyjzPuw5MT5VbLbOq8OI0lc95XVmLV4/9S4iBoadnMo8z1oz4KLugH3CrMDdW8urBfwHzAIiHrM/Oupl5LF2xX58iCs4DPrJNXIsBvKW4Q4KkTvn8DD82owoJUV7U3FRBqjs5Fg2gd98eCMps2+PVY4aqJoUoJVV+IQkij9PO5eSl9k8PRUDK2HbskDy4xqiqUpI7pyK+rCmadUncPlk0tW/8NgUSOpKgT5B7yw4bW8d3KRehbVwpb9tV74P0ZD0Ns+BLFEB8x3HsMRHs1CRFiTecb6V0k/Ro2dRX8UNtZHDvpNt8dug7rSccIIEiuCAwTa1qr+7b5NFy6jRBSaW8sZJmml8rN3Macuu81ZYnJNTYmcagGkY5dkogfqvdUiYfvrpwY0npJjhaxMvbmpvq8kJGF153TCyNjxNXQBYHgpUqfTMiNelIbfUni7iQuvv+NlxBOPXO6SJOOXL1NdL4QHDBoetvHhIvq5xiWm+NfZ5xDjcBEhvTcMSRc+aNG8qQvv0JnTPB/g5tDX5fUJQKW/UEJVeVG2xsfEsmFpcFt8aicahttNgc1uCYHC8Jrw+73GbkU1Iu2IXIkarG9ii3xvVl1EgprYxStzvDR2ylF1UEA3J+oSboWtSaG0Z4oHry5Ud6Zp0ywSG4DBtKmLqVqa1qwz1Bx7t8fn0GQoa5zJUqrmhIN4VYtT/NiMUFu95pM7clH8X6Ku+dTCakkIlDcnV1JBRZHlDixXaMyMI0jaiOZThQO23FdVjEG5kLS92GCz2zpzkhibTQqrx5Ri8H5BEyltQTxoo49dlIRkYexNvruIHzjPJSLVozs7TcRAwEim27QeTmJZbWJ4w0vUpFXJKz3DDbRMZNXDTrkI/68xnY1/LGsHSjMl0aE8puaoozwlhlK/dDOCdh6kzz+5qGakJBjkcYIy1fMV/JQFioJdjcHKYwDolUynnzdB8xd4hM9g+yBm8ovIbEZ3gvSdhZ6G3WIzPFUBFsCE+fVKPMvEiolDXHZB9reX9oA6zxCRW3jyEtpX+Npf9p2JO7ezLsx6HMFs9twY1cy3dbC4rkBGa3x31qSVAlUhlh3kN6fdh3m4umEzJWaLL3dIWphYh/95qVJOgrjZNxxm/vJqqwX1dMhn3V1v1yH9HTY2PmBjt40PVieSRIOCPnw1F9tr+0U+RS2oX9K+ZTV0vJNrEYv3UP0aGY0TAarMeijNQWlJQ027rIxqXayxD+bdhXU2hpedykDWRYv9aS+EiQUH/yNpe6rWpSle4TSlXYR2lxF/bVFFqaVhIdga4fBJ51KlVVa9xAOY53IEQ9Az1S2/5BtYkeb2WYga5vKBpwfLSsx7sqKKpMUeb6vWdUTw8tqriYLvOHYb+fwqWO3U47qLOI9GymEy6VjGTok/O7pXtpttHzXah+CiuqZVrxpl7MZZeBeixQZXeJ1IXI2YPMHT5vCpaX+lZoGo74abqKToZXUgRcjXcHgseP77AdjLTQ+JkTmQayoxaR9pjaSmvzmzU198F6dA/xxNDXrUfmBeRCRIfMcIKIV8S+dKJ3pwzbAjiPn/WBw0s9o/0BnCHaya3Wl4lCJlN955G8/AhBlPHtQeGDn0l1uI7wFOoed3dbbQVs7BPx7AJMW6sbpjc7Mz0i/brxXgIrM1Vmqa7LEEifV8smUwqChtvbeTQzqlv3yCvM4fAqqQqNyedz0RoecGhuRGlE9A0VHUqGSLeMZCbxr8upxT5Qsbi0VTMTmr81tLEOTwZnXzq21e59ECLYnIoXS92nUF3m6otkWxd97f7STIZHkcR9XRgHnP3wCH8X7YghSx3XWgEiX2yvt8tcoLCY5li6GU3TpgtUHBH1PPJE3cz76pWC9SYBy2KLsRhAG6nomF8syBfNtEO7r7o7DDpWZ26wVgtRhYkNRXc1jU8x2debQbzKrpoLqJnoYr6W+2O/i66mKEPaXO3E66y/fhk7OPRUkJMpBnNV8lsoD3NXUMSQ3vrkF2ELNFJliw9ASP/o9ivYdfcq1X6wDP76h7dfAUgzVdYpFyIZ0LOr34BcgJ11Ll00nEsT34LaoTHeDl5Vw13A/wAhE536kgy5ljssv40IOvGtetOxIbd+fo4J511Zfq/MVOPK4G8g6uv4k2PV5g/EQcqk5Jv5xeCwdREbyuPAv4b1n7sQD2gDmejre1r7v4BMFv/QiHiE6sET/pEZ1BGtR5B+z3P+p9iMtGjA9oOQZmoN5D7vrwJ9YYdm2Ni6TI/WAT+GuUwW/2hVo14N/DPz/DPm/j+vHsJf/MVf/MV/g/8BXS2UJfJiBNIAAAAASUVORK5CYII="
  retrievedImage4="data:image/png;base64,";
  retrievedImage5="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAkFBMVEX///+pqakAAAAfGhetra3V1dWurq4XEQ0QBwAMAAAdGBUNAAB0c3FxcG4IAAAVDgkRCQE2MjA8ODZCPz0wLCqenZxta2ksKCVRTkxeXFpHREKlpKR5d3bu7u0oIyGFhIO+vbzm5eVMSUeTkpJhX17Ew8Pb29qOjYyFg4JXVVOXlpXNzMv29va3trbBwMBgXltIq2GZAAAVVklEQVR4nO1daXejuBINJQUJGZDYwWC8YWNsnPn//+5J4H3JJN2TZ9Gn7zlzOnH8QXdUqrpVkkpvb3/xF3/xB2Fn7NevHsOPYgUS1J61rx7IjwEDQoiMgBW7Vw/lh9AGlAWNT7gFQTR59Wh+BBMfmBc7acI5kGL+6uH8CAogJLWLOEDEgub91cP5CcxCIElm23GaEAHNn7ggJxsQxK9s22nCP5XjOgXO/FhyzEIuoPoTQ+SHB5wojnam5tEeoF+d58u6XkbGx3ryOLybHUdpq3ZTSp9j/J/H99uQ4sV1oUeZFtuP+R3P1vAlRy9zbCctCaQDc6tLKV6SwPcSRDgXluTpZfXqJv61+0ByTNJYxg75peg1Q/1FTGRIQGGqnEnVpH4peVIXiD9dXX/vYwEWQf4/RSWnsRrWasxDsDhHQRM7dlHYcROo+ZSWG1+TXBcMBGfjzGcUBiZydoUvEwnOS3+RxbGk6fyzCBGh4BdXkzXBjQuUSE0O+1eN9VfRrqO4lD6HE1J6/qKp4rhKPUTAvvniOpIRUjKcvWKUv4t2jTeBcqnSZglDZVkixMr7700qwsjArPQS61kUB2zUBRD+kGEbMuYNPjNu5++71crhjN17zRUg3rxgTD+BqcXEvRItKHLrF4zmJ6C43AsYxhCsHnx7iHAEgruMaSdF0NDC4VNU/AHDzQix8BWj+QlkjxjKQEI3rxjNT6Ah9ytuDUON94+QSoYfN5/liuGwhPcnUAxnN581HBH/FYP5ETxg2CKG3OVLRvMTODOczCWUbXax4s+puSlPs3vbRdU4IaMRSRbObGpJrTp4UXqCWnNpopINQoiwLNdVmZNwXj2u38N8tZ+tD5MkIz7iAnmLhcwYi3pajEsmjdR87Qh/C/Ol1xXdRoWUo+2yZJyM7XiBCBeUksXUrIXKGWevHucvI6KSHfISRbLACQhURbXPLRBBGlCwrECuTEQI5K8e6a+hrQDYdt227WQ/BgsIiaN9zAXEq7Z9a9tVDFwS5HHAh1cW7pACbE5eci8Ir02zct3xOUNc+6400sgY80GmTzZczcw64UnuWDelqBhYYBqGx8vhCbcVwHUpe22RALnxzddii20xjggU/7+h/UcI3ermEwMYuUsE25CPsYFtC4a20TYD627IAYH7GsYO6BRLO3VvK6m6I4PbKZTeBm5tVCGlahILAcPSbpPyLhGUFkkfSewV8Mgw8nJg0uYd3JNzPM/Nw82Jlo8KbJiplf0fxvXfYQ+o/2G+8RPfvlqS75t0kV2cGqrcSpmpNawUI4I+b58pwUboReBoY7Vhw+Gs1GoIJMMlcQflTbcwVv/sIGrbrUwBT8G/9aGMK0bkR8cCWw6hdKbGo5WrMXLw1D+8S/1ijk77FQ14c2moXdZ0mEUTSsnQ9GBQm93SQb4pG92qX2pL0ul+kp+4HY9CZU2HEpvRzaE5hkFVTdeu0ihFb6uByiB6TzkmfdnJ4Wrjt5+0LfiKYTqwmO+r4S+I8PM8UWzUDuEkn1PGmNQ1M8HUcdPeG9luphg2d5pVb0yVM10QRGSOiA4Ml+MVKONcBIfPrO6rycgeIkOZWuzVXtMRvHmbuLym6mdC+s9YpwreYbQdopVmnJTtHk4MYfkWU5YwdAFmqRjvWL6MFYPzNDNgjKZqX+lAhk+2QNgVQcSU7pkDLVQ4HFq0CMR4St3GPNgjonkEPPWuGQpllo0IczWHMuLPXj3qb2AP7ta0hfAXPSfSZEAX5oZeMVTV/CXQjZpCY8seJI/6IhCpaeAN4adJ425mGLgRFwRppvQrzUxFENf0wTENbbECayknBkcL4EyBuOG0m6mAn72rzCVsEEH3uYEdy3v1sL+BLm3vHGQUB0lZelltHoiM6WFaWbA2XJem/efSlQ4pWMxBbA4DN7Bpqv+Ovxo4ZgfTXQgQzDn+IU+GdHhvCwf3+Ah4m6GR2nuiFsoi8/hpTYe01e3RynzK0MA4n8ZN2sTT/DSzhlnRAS3Dd+BL/JxhR7LD5UfhkOL9FpLPCT6gXFtD2glurOwTI30IMyWM3hdYNcUEWdPvzmE0Yj4D/OqhfxHvAM896ZMpjDlyfDKU/acIyu8aaU7I2HYY8dVusYwhmgvUAtJvMlRTGFe+TLEotSh1AZJa5+OYjRt/cxlGiARjQqykcTb11M58SsHdaMux9aziewzNiqOS0HCT92HSNLYxGrlM1wDZcqv+FkO8lPkGE/GlAsCGzSgs9PQ8E5n8fo9hQBBj09PaNVe71d7AecMp1/LAm2QYfYegWYxkMrw5Edy/d9gb5lSmzbNX03mA+fcY4kim/eJCqe96hu8f8k8J1zGl+uYcYl/aaHLx+/v7mWLu6XjQRjL8l8ziEmYllI1epFEnhu9qMfpcvzMa7XdkKS4EL1l4+cmZ4buy4ZCHunnUNnDtrzLESzaqfHGVL+/ODGeqIsD1286I3a8mTzhCPJwDvYqfq4tJzJWrdbXzNksIv8YQ5wnn8x2UV57pYiG+d9X+lHPN7HT3xexJeUpr95Z3+6Nn5B+XvsZQqlW3OuMk+ZIwxZH0k7O3tw0sbqZ8d80Q20K3i1+O6/+7mSov2W3FxG5z8218pLg+/I/yLM2czQ74v2pvc8l4f7oke+CYDhR3vbXjglqaTeL4uBnxfAYLS7BeVmfuP3cM873iuDv9XsL0xZRusAPx6UqUeYPl+odpubfSjqO5P39qxpZuN6ErF32SQZl1KKA6Drm48zT3iEA3eTopeZI/oWjmGRcX1w8iCD7bAlit5F9N39Vtj//d5WX0iCI2HCYguFDTM0g+2cY5KHDH8jUz07cV5aTANxwxzm1EgV3dH1kDeW7RHwcFvnV1C4nS25QuDWrjtHGIsZnXjeQnNjca7HAW4yF2RwWuY2uQSQyC+nEdYdM0cbQs0pBSgOndXDTWI2fa4xAXDTwGHW9h7sYAMuyVoReWiI8sAN94sJry5wsRHxmamX45VIf11CeHllE8yYzHS2kO7lMJdJrD2Ar+z2P/MuZyFeXGbPeJo/jETHdHhoX1oCnKYGCCeGams4OnwRuLv3qYv4G2pPGnkygX5GYkXj3M38ESyNOgv3vfmUqpj9CrR/k7mCDx9PBGbijyStS8epS/hS2Qz2usZnV3E25gSHjwGUEV8QfehWgH1Pk0h2IaqrbvYfNJ2FfHbdwhnQl7iHYs2MN0q1+GzWj4zcDmiIenpBnfBI98dEqZ2ydNUQeAHSfegSJelv61J+VcCu/1vkj9MAn91DE0P4/yGA0hSW+oOOPuxf5Vt9EPta9av7rWyHJVB1i0GR7JjCNG+u38gPHT0VppsiErS+RSkYzjzXQ63cSLkFAXxrNXD/mbiNWRDKtSlloycko3cK4uhjGOmjo38fk8ii8ojDWrwP0L1BUi5vKwwAZBZHxgaG491RyEV5F56WoxNurUEhBrV775BBuK3CiCkQhtgphn9rWdSl27of6DSILxMuB0NCAlsHQRGG/zGFyqetZEeVQXi5G6vUAr82GoxGbAhtRuwoC+oLYu1BVa1vlNgkq5BIsngs6sKPEFpLrLnXa+3s1wnhenloP9LUXVJ5uj5BOCjkUzM6aWp+1inOz2dTUOWd/gXJxaYO5Axo3JfNK2Y4Ke5v9mQflC/rPhotSQYrvLq9KSMZwKTlST4SQsTww/JEPa/bQH8mzfRhHsLy7WTDeKk49aNVCmXK6yMEizKnYKpxrLpbfovzBTDDv5WQn0pL6BY0sS7ByQuWTC02ctrqPMVU2wkZSX/8SOLeHEja+afZfxQYgphiPFcAWjzWMvirORCI5K3ay5lb6U1Qmt2YBqw+4tsp6c6kzvJ0zSg+ZcD98rhmpSfDF+TDAKKE3P+3fmhrhaUFwlqrlXkPXk4qoJQqlWVMezyry0siPDHYiHabE5LYnqFnJmvCRIh7sMayDMy2LJLF3IiePdkwI0qfLbLGF/sNLTJb/rCcxTl5DNhQPCEWGEabD/NgMmo4AkxoU6jw8sqOqPRycPD+twbYn7M4DYKBCnwaWKw1FCyjqhGtjptGu+B1Y5rorlfn3/tMcBH70v3UL5gJ9HObONS4K5T0ht1q4Onfta1bZUBvJ/+ZrUNEy8vS2s6zqx1OF2KLjbXMlwdQZV5ctmI3Q7//YcStPwt7l7WX6TWWHdEMHpYntNOwq52y3KHLmDaRP6rlTbGwZuHriZeVQ0zOIUNdvrNANvpTfuvQ4u6GDuu89BvaFQuOnexHm0ncaNJ6gQPLCjmzTKnEonenJHPh1KzXECqrHLmHuLwA+l5xTS95KFExm3nhXHMr6edjzwVAxmEiXD5s1SF71GqrSWpHad3x5dUT5mLER4djs4JULv/Zt2vtvnyyKuKoaoPQfSZLG96Q5z3NOTy3MqJUNz/osZjwjTtV/YZI3r2O+PNLgW5Qi5WxkysHl7H/piBUapxdFFcmwWrshCV7NDjG/dYzRFyhQxmSfKbMoPxgtPtZAywH/MrTfQmHF3HJ0J4pryADtWolfRf51XoTpzwznyxqmUrI5dyFSqlFn/ZAuP8wo1XYadCIEuT5XhJSNhjnNLpxYw75uyT4NVLmX3udQ/i0QlG6jevU2heZIa5nEp/59U+VVygQiSPgcvtDlZ1BqJyoNLv6l6cnHcjEPChQtl1VWyC3hwbUPO2jLjgpMmuvyjItid/8MbS5Mmoe+hyqQWVfdqWVylY6/s2LmLze4gLp07hn0xfySVTXat3MyaHcNizvQw09YXqFw0TToOvLBkhAjV1bypPy6k8xVDKdzMaJp5XAjXc6JrByszfFIe4r40Uy0qxK0vp5CcEkWgTX33pLWy0m4XRiKqiyZkUtu4rFreKhszFrw8HlE1K67H0aLJJggTtdcZT42P9cOVM4XxtCicOBsnIxkjhXSeflybt/V9nC8s4R+9Dq75cN4dWrrMUlCqlEp/G9fRA2ljLhMpbE50I8SGc2pj66Iy8fyxlG6bZWQ8Em7SXqX0JqfLgDgPSeJpd+j9CXLCyjzPuw5MT5VbLbOq8OI0lc95XVmLV4/9S4iBoadnMo8z1oz4KLugH3CrMDdW8urBfwHzAIiHrM/Oupl5LF2xX58iCs4DPrJNXIsBvKW4Q4KkTvn8DD82owoJUV7U3FRBqjs5Fg2gd98eCMps2+PVY4aqJoUoJVV+IQkij9PO5eSl9k8PRUDK2HbskDy4xqiqUpI7pyK+rCmadUncPlk0tW/8NgUSOpKgT5B7yw4bW8d3KRehbVwpb9tV74P0ZD0Ns+BLFEB8x3HsMRHs1CRFiTecb6V0k/Ro2dRX8UNtZHDvpNt8dug7rSccIIEiuCAwTa1qr+7b5NFy6jRBSaW8sZJmml8rN3Macuu81ZYnJNTYmcagGkY5dkogfqvdUiYfvrpwY0npJjhaxMvbmpvq8kJGF153TCyNjxNXQBYHgpUqfTMiNelIbfUni7iQuvv+NlxBOPXO6SJOOXL1NdL4QHDBoetvHhIvq5xiWm+NfZ5xDjcBEhvTcMSRc+aNG8qQvv0JnTPB/g5tDX5fUJQKW/UEJVeVG2xsfEsmFpcFt8aicahttNgc1uCYHC8Jrw+73GbkU1Iu2IXIkarG9ii3xvVl1EgprYxStzvDR2ylF1UEA3J+oSboWtSaG0Z4oHry5Ud6Zp0ywSG4DBtKmLqVqa1qwz1Bx7t8fn0GQoa5zJUqrmhIN4VYtT/NiMUFu95pM7clH8X6Ku+dTCakkIlDcnV1JBRZHlDixXaMyMI0jaiOZThQO23FdVjEG5kLS92GCz2zpzkhibTQqrx5Ri8H5BEyltQTxoo49dlIRkYexNvruIHzjPJSLVozs7TcRAwEim27QeTmJZbWJ4w0vUpFXJKz3DDbRMZNXDTrkI/68xnY1/LGsHSjMl0aE8puaoozwlhlK/dDOCdh6kzz+5qGakJBjkcYIy1fMV/JQFioJdjcHKYwDolUynnzdB8xd4hM9g+yBm8ovIbEZ3gvSdhZ6G3WIzPFUBFsCE+fVKPMvEiolDXHZB9reX9oA6zxCRW3jyEtpX+Npf9p2JO7ezLsx6HMFs9twY1cy3dbC4rkBGa3x31qSVAlUhlh3kN6fdh3m4umEzJWaLL3dIWphYh/95qVJOgrjZNxxm/vJqqwX1dMhn3V1v1yH9HTY2PmBjt40PVieSRIOCPnw1F9tr+0U+RS2oX9K+ZTV0vJNrEYv3UP0aGY0TAarMeijNQWlJQ027rIxqXayxD+bdhXU2hpedykDWRYv9aS+EiQUH/yNpe6rWpSle4TSlXYR2lxF/bVFFqaVhIdga4fBJ51KlVVa9xAOY53IEQ9Az1S2/5BtYkeb2WYga5vKBpwfLSsx7sqKKpMUeb6vWdUTw8tqriYLvOHYb+fwqWO3U47qLOI9GymEy6VjGTok/O7pXtpttHzXah+CiuqZVrxpl7MZZeBeixQZXeJ1IXI2YPMHT5vCpaX+lZoGo74abqKToZXUgRcjXcHgseP77AdjLTQ+JkTmQayoxaR9pjaSmvzmzU198F6dA/xxNDXrUfmBeRCRIfMcIKIV8S+dKJ3pwzbAjiPn/WBw0s9o/0BnCHaya3Wl4lCJlN955G8/AhBlPHtQeGDn0l1uI7wFOoed3dbbQVs7BPx7AJMW6sbpjc7Mz0i/brxXgIrM1Vmqa7LEEifV8smUwqChtvbeTQzqlv3yCvM4fAqqQqNyedz0RoecGhuRGlE9A0VHUqGSLeMZCbxr8upxT5Qsbi0VTMTmr81tLEOTwZnXzq21e59ECLYnIoXS92nUF3m6otkWxd97f7STIZHkcR9XRgHnP3wCH8X7YghSx3XWgEiX2yvt8tcoLCY5li6GU3TpgtUHBH1PPJE3cz76pWC9SYBy2KLsRhAG6nomF8syBfNtEO7r7o7DDpWZ26wVgtRhYkNRXc1jU8x2debQbzKrpoLqJnoYr6W+2O/i66mKEPaXO3E66y/fhk7OPRUkJMpBnNV8lsoD3NXUMSQ3vrkF2ELNFJliw9ASP/o9ivYdfcq1X6wDP76h7dfAUgzVdYpFyIZ0LOr34BcgJ11Ll00nEsT34LaoTHeDl5Vw13A/wAhE536kgy5ljssv40IOvGtetOxIbd+fo4J511Zfq/MVOPK4G8g6uv4k2PV5g/EQcqk5Jv5xeCwdREbyuPAv4b1n7sQD2gDmejre1r7v4BMFv/QiHiE6sET/pEZ1BGtR5B+z3P+p9iMtGjA9oOQZmoN5D7vrwJ9YYdm2Ni6TI/WAT+GuUwW/2hVo14N/DPz/DPm/j+vHsJf/MVf/MV/g/8BXS2UJfJiBNIAAAAASUVORK5CYII="
  selectedFile: File;
  retrievedImage: any;
  base64Data!: any;
  retrieveResponse: any;
  message: string;
  imageName: any;


  picture={
    id:"",
    title: "",
    description: "",
    cameraType: "",
    cathegory: "",
    keyword: "",
    pictureDate: Date.now(),
    width: "",
    height: "",
    maxAmountPublic: "",
    salesPrice:"",
    pic_byte_ny: Blob,
    after_conversion:""

    };

  choices: Choice[] = [
    {value: 'newest', viewValue: 'Nyast först'},
    {value: 'oldest', viewValue: 'Äldsta först'},
  ];

  search : String ="";

//kanske bort

  constructor(public pictureService:PictureService,
              private dialogRef:MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              private sanitizer: DomSanitizer,
              private httpClient: HttpClient) {

    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();


    this.dateFrom = new FormGroup({
      start: new FormControl(new Date(year, month, 13)),
      end: new FormControl(new Date(year, month, 16)),
    });

    this.dateTo = new FormGroup({
      start: new FormControl(new Date(year, month, 15)),
      end: new FormControl(new Date(year, month, 19)),
    });


  }


  ngOnInit(): void {
    //this.retrievePictures();
    this.title="";
    //console.log(this.retrievedImage6);
    console.log(this.retrievedImage5);
    console.log(this.retrievedImage4);
    //this.getData();

    //console.log(this.base64Data1);
  }

    getData() {
      this.pictureService.getAll().subscribe(
        response =>{this.pictureService.pictures = response;}
       );

    }


  retrievePictures():void{
    this.pictureService.getPictures()
      .subscribe(
        data=>{
          this.pictures=data;
          console.log(data);
          },
        error=>{
          console.log(error);
          });
  }

  refreshList(): void {
        this.getPictures();
        this.currentPicture = undefined;
        this.currentIndex = -1;
      }

  setActivePicture(picture: PictureTest,index: number):void{
    this.currentPicture=picture;
    this.currentIndex=index;
  }

  setCurrentPicture(picture: PictureTest, index: number): void {
    if (this.currentPicture && this.currentPicture.id == picture.id) {
      this.currentPicture = undefined;
      this.currentIndex = -1;

    } else {
      this.currentPicture = picture;
      this.currentIndex = index;
    }

  }
  removeAllPictures(): void {
      this.pictureService.deleteAll()
        .subscribe(
          response => {
            console.log(response);
            this.refreshList();
          },
          error => {
            console.log(error);
          });
    }


    getOnePicture(){       /**searchTitle**/
      //this.currentPicture=undefined;
     // this.currentIndex=-1;
      this.pictureService.findPictureByTitle(this.title)
        .subscribe(
          pictures=>
          this.pictures = pictures);

    }

    getOnePictureByCathegory(){       /**searchTitle**/

          this.pictureService.findPictureByCathegory(this.cathegory)
            .subscribe(
              pictures=>
              this.pictures = pictures);

        }
      //this.retrievedImage = 'data:image/png;base64,' + this.base64Data;;


  onSubmit() {
    this.getOnePicture();
    this.getImage2();
    console.log(this.picture.pic_byte_ny)

  }

  //koden från details:


    getPicture(id: string): void {
      this.pictureService.get(id)
        .subscribe(
          data => {
            this.currentPicture = data;
            console.log(data);
          },
          error => {
            console.log(error);
          });
    }

  updatePicture(): void {
      this.message = '';
      this.pictureService.update(this.currentPicture.id, this.currentPicture)
        .subscribe({

        next: (res) => {
          console.log(res);
          this.message = res.message ? res.message : 'Bilden är nu uppdaterad!';
        },
        error: (e) => console.error(e)
      });
  }
    deletePicture(): void {
      this.message='';
      this.pictureService.delete(this.currentPicture.id)
        .subscribe({

          next:(response) => {
            console.log(response);
            this.message = 'Bilden är nu raderad!';
            //this.router.navigate(['/pictures']);
          },
          error: (e)=>
            console.log(e)
          });
    }
    getPicturesByTitle(title: string): void {
        this.pictureService.findPictureByTitle(title)
          .subscribe(
            data => {
              this.pictures = data;
            },
            error => {
              console.error(error);
            });
      }

    getPictures(): void {
        this.pictureService.list()
          .subscribe(
            data => {
              this.pictures = data;
            },
            error => {
              console.error(error);
            });
      }



        searchPictures($event: any) {
          if ($event.keyCode === 13) {
            $event.preventDefault();
            $event.target.parentElement.submit();
          }
        }

    /**blobToBase64 = (blob) => {
    return new Promise( (resolve, reject) =>{
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onloadend = () => {
            resolve(reader.result['split'](',')[1]);
            // "data:image/jpg;base64,    =sdCXDSAsadsadsa"
        };
    });


};**/

//url=this.blobToBase64(this.picture.picByte);
url1="";



/**getImage() {
    //Make a call to Sprinf Boot to get the Image Bytes.
    this.httpClient.get('http://localhost:8090/picture/get/title/' + this.picture.title)
      .subscribe(
        res => {
          this.retrieveResponse = res;
          this.base64Data = this.retrieveResponse.picByte;
          this.retrievedImage = 'data:image/png;base64,' + this.base64Data;
        }
      );
}**/
getImage2() {
    //var reader = new FileReader();
    //Make a call to Spring Boot to get the Image Bytes.
    this.httpClient.get('http://localhost:8080/picture/find_1/{title}' + this.imageName)
      .subscribe(
        res => {
          this.retrieveResponse = res; //this is blob i think
          //reader.readAsDataURL(this.retrieveResponse)

          this.base64Data = this.retrieveResponse.picByte;
          this.retrievedImage = 'data:image/png;base64,' + this.base64Data;

        }
      );
  }

getImage3() {
    //var reader = new FileReader();
    //Make a call to Spring Boot to get the Image Bytes.
    this.httpClient.get('http://localhost:8080/picture/find_2/{cathegory}' + this.imageName)
      .subscribe(
        res => {
          this.retrieveResponse = res; //this is blob i think
          //reader.readAsDataURL(this.retrieveResponse)

          this.base64Data = this.retrieveResponse.picByte;
          this.retrievedImage = 'data:image/png;base64,' + this.base64Data;

        }
      );
  }

}
