import {Component, Inject, OnInit} from '@angular/core';
import {Customersmodel} from 'src/assets/models/Customersmodel';
import {CustomersService} from "../../../services/customers.service";
import { Router } from '@angular/router';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";


@Component({
  selector: 'app-dialogcustomers-edit',
  templateUrl: './edit-customers-component.html',
  styleUrls: ['./edit-customers-component.css']
})

export class EditCustomersComponent implements OnInit {

  modifyCustomersForm: FormGroup;

  selectedCustomer: Customersmodel = {
    id: 0,
    company: "",
    contact: "",
    email: "",
    discount: null,
  };

  isEditing: boolean = false;

  //Loads data directly in constructor into form group which is based on customersmodel
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialog: MatDialog,
              private customersService: CustomersService,
              private router: Router,
              private currentDialog: MatDialogRef<EditCustomersComponent>,
              public fb: FormBuilder,
  )
  {
    this.selectedCustomer = this.data
    this.modifyCustomersForm =
      new FormGroup({
        id: new FormControl(''),
        company: new FormControl(''),
        contact: new FormControl(''),
        email: new FormControl(''),
        discount: new FormControl('')
      });
  }

  ngOnInit(): void {
    //Adds current data in dialog when editing row
    if (this.selectedCustomer != null) {
      this.initForm();

    }
     this.getAllCustomers();
  }

  /**onSubmit() {
     {
      this.saveCustomers();

      return;
    }
    this.addCustomers();

  }**/

  onSubmit() {

    this.addCustomers();

  }

  getCustomerToDialog() {

  }

  initForm() {
    this.modifyCustomersForm.setValue({
      id: this.selectedCustomer.id,
      company: this.selectedCustomer.company,
      contact: this.selectedCustomer.contact,
      email: this.selectedCustomer.email,
      discount: this.selectedCustomer.discount,
    });
  }

  alertDialog(msg: string) {}

  getAllCustomers() { //Adds current data in dialog when editing row
    this.customersService.getAllCustomers(this.selectedCustomer.id).subscribe((data: Customersmodel[] | null) => {
      if (data == null || data.length == 0) {
        this.alertDialog("Failed to get customers");
        return;
      }
    });
  }

  addCustomers() {
    let f: FormGroup = this.modifyCustomersForm;

    let request: Customersmodel = {
      id: f.value.id,
      company: f.value.company,
      contact: f.value.contact,
      email: f.value.email,
      discount: f.value.discount
    };

    this.customersService.addCustomers_1(request).subscribe(data=>{
      /**if (data.iserror) {
       if (data.message == "No customers") {  }
     }**/
      this.closeDialog();
    });
  }

  getErrorMessage(){}

  closeDialog() { this.currentDialog.close(); }

  saveCustomers(){
    let f: FormGroup = this.modifyCustomersForm;

    let u: Customersmodel = {
      id: f.value.id,
      company: f.value.company,
      contact: f.value.contact,
      email: f.value.email,
      discount: f.value.discount
    }
  }


}

