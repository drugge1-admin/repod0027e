import {Inject, Injectable, LOCALE_ID} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {BehaviorSubject, catchError, Observable} from 'rxjs';
import {CustomersAddResponse, Customersmodel} from 'src/assets/models/Customersmodel';
import {environment} from "../../environments/environment";
import {HttpClientService} from "./http-client-service";
import {CustomersRequest} from "../../assets/models/CustomersRequest";
import {CustomersResponse} from "../../assets/models/CustomersResponseModel";
import {FormControl, FormGroup} from "@angular/forms";
import { HttpRequest, HttpEvent} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class CustomersService {

  private apiServerUrl=environment.apiBaseUrl;

  modifyCustomersForm: FormGroup;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private httpClientService: HttpClientService, private httpClient: HttpClient, @Inject(LOCALE_ID) public locale: string) { }

  getCustomersByCompany(company: string): Observable<Customersmodel[]>{
    return this.httpClient.get<Customersmodel[]>(`${this.apiServerUrl}`);
  }

  public addCustomers(customersRequest: CustomersRequest) : Observable<CustomersAddResponse> {
    return this.httpClientService.httpClient.post<CustomersAddResponse>(
      `${this.httpClientService.apiServerUrl}/createCustomers/${this.locale}`, customersRequest).pipe(
      catchError(this.httpClientService.handleError<CustomersAddResponse>('addCustomers:CustomersAddResponse'))
    );
  }

  public getCustomersToTable(): Observable<Customersmodel[]>{
    return this.httpClientService.httpClient.get<Customersmodel[]>(`${this.apiServerUrl}/customers/all`);
  }

  public deleteCustomers(id: number): Observable<any> {
    return this.httpClientService.httpClient.delete(`${this.apiServerUrl}/customers/${id}`);
  }

  getCustomersList(customersRequest: CustomersRequest): Observable<CustomersResponse> {
    return this.httpClientService.httpClient.post<CustomersResponse>(
      `${this.httpClientService.apiServerUrl}/getCustomersList${customersRequest.id}`, customersRequest).pipe(
      catchError(this.httpClientService.handleError<CustomersResponse>('getCustomersList:customersResponse'))
    );
  }

  getCustomersById(id: number): Observable<Customersmodel>{
    return this.httpClient.get<Customersmodel>(`${this.apiServerUrl}/customers/${id}`);
  }

  public getAllCustomers(id: number) : Observable<Customersmodel[]> {
    return this.httpClient.get<Customersmodel[]>(`${this.apiServerUrl}/customers/${id}`);
  }

  dataChange: BehaviorSubject<Customersmodel[]> = new BehaviorSubject<Customersmodel[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  get data(): Customersmodel[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }
  //Irinas kod
  createData(formData: FormData): Observable<any> {
      return this.httpClientService.httpClient.post(`${this.apiServerUrl}/customers/customers`, formData);
    }


  public addCustomers_1(customer: Customersmodel):Observable<Customersmodel>{
  return this.httpClient.post<Customersmodel>(`${this.apiServerUrl}/customers/customers`, customer);
}


    //slut på Irinas kod
}

