
import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http'
import {BehaviorSubject, catchError, Observable} from 'rxjs';
import { Invoicesmodel } from 'src/assets/models/Invoicesmodel';
import {environment} from "../../environments/environment";
import {HttpClientService} from "./http-client-service";
import {Customersmodel} from "../../assets/models/Customersmodel";

@Injectable({
  providedIn: 'root'
})

export class InvoicesService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  private apiServerUrl=environment.apiBaseUrl;

  constructor(private httpClient: HttpClient, private httpClientService: HttpClientService) {
  }

  getInvoices(): Observable<Invoicesmodel[]>{
    return this.httpClient.get<Invoicesmodel[]>(`${this.apiServerUrl}`);
  }

  public createInvoices(invoicesmodel: Invoicesmodel): Observable<Invoicesmodel>{
    return this.httpClient.post<Invoicesmodel>(`${this.apiServerUrl}/invoices/invoices`, invoicesmodel);
  }

  public getAllInvoices(id: number) : Observable<Invoicesmodel[]> {
    return this.httpClient.get<Invoicesmodel[]>(`${this.apiServerUrl}/invoices/${id}`);
  }

  //Switched object in observable from Invoicesmodel to any
  getInvoicesById(id: number): Observable<any>{
    return this.httpClient.get<any>(`${this.apiServerUrl}/invoices/${id}`);
  }

  updateInvoices(id: number, invoicesmodel: Invoicesmodel[]): Observable<Object>{
    return this.httpClient.put(`${this.apiServerUrl}/${invoicesmodel}`, invoicesmodel);
  }

  public deleteInvoices(id: number): Observable<any> {
    return this.httpClientService.httpClient.delete(`${this.apiServerUrl}/invoices/${id}`);
  }

  dataChange: BehaviorSubject<Invoicesmodel[]> = new BehaviorSubject<Invoicesmodel[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  get data(): Invoicesmodel[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  public getInvoicesToTable(): Observable<Invoicesmodel[]>{
      return this.httpClientService.httpClient.get<Invoicesmodel[]>(`${this.apiServerUrl}/invoices/all`);
    }
}

