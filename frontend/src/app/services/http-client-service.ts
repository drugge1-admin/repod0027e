import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { AlertService } from './alert-service';
import { environment } from "src/environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})

export class HttpClientService {
  public apiServerUrl=environment.apiBaseUrl;

  constructor(public httpClient: HttpClient, private alertService: AlertService) { }

  private log(message: string) { console.error(`HttpClient: ${message}`); }

  public handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.alertService.error(error.message);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
