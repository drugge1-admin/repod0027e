import { Injectable, OnDestroy } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import {Observable, Subject, Subscription} from 'rxjs';
import { filter } from 'rxjs/operators';

import { Alert, AlertType } from 'src/assets/models/alert.model';

@Injectable({ providedIn: 'root' })
export class AlertService implements OnDestroy {

  private subject = new Subject<Alert>();
  private keepAfterRouteChange = false;

  private routerEvents: Subscription;

  constructor(private router: Router) {
    // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
    this.routerEvents = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.keepAfterRouteChange) {
          this.keepAfterRouteChange = false;
        } else {
          this.clear();
        }
      }
    });
  }

  ngOnDestroy() {
    this.routerEvents.unsubscribe();
  }

  onAlert(alertId?: string): Observable<Alert> {
    return this.subject.asObservable().pipe(filter(x => x && x.alertId === alertId));
  }

  success(message: string, alertId?: string) {
    this.alert(new Alert({ message, type: AlertType.Success, alertId }));
  }

  error(message: string, alertId?: string) {
    this.alert(new Alert({ message, type: AlertType.Error, alertId }));
  }

  info(message: string, alertId?: string) {
    this.alert(new Alert({ message, type: AlertType.Info, alertId }));
  }

  warn(message: string, alertId?: string) {
    this.alert(new Alert({ message, type: AlertType.Warning, alertId }));
  }

  alert(alert: Alert) {
    if (alert.keepAfterRouteChange == null) { return; }
    this.keepAfterRouteChange = alert.keepAfterRouteChange;
    this.subject.next(alert);
  }

  clear(alertId?: string) {
    this.subject.next(new Alert({ alertId }));
  }

}
