import { Component, OnInit, Inject } from '@angular/core';
import {Invoicesmodel} from 'src/assets/models/Invoicesmodel';
import { Router } from '@angular/router';
import {InvoicesService} from "../../services/invoices.service";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-dialoginvoices-add',
  templateUrl: './invoicesadd.dialog.html',
  styleUrls: ['./invoicesadd.dialog.css']
})
export class InvoicesaddDialogComponent implements OnInit {

  modifyInvoicesForm: FormGroup;

  public id: any;

  private invoicesmodel: Invoicesmodel[];

  invoicesmodel_1:Invoicesmodel={
    id: 0,
    company: "",
    name: "",
    date: Date.now(),
    cost: null,
    state: null,
  };

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private invoicesService: InvoicesService,
              private router: Router,
              private currentDialog: MatDialogRef<InvoicesaddDialogComponent>,
              private dialog: MatDialog,
              public fb: FormBuilder,
              ) {
                     this.invoicesmodel_1 = this.data
                     this.modifyInvoicesForm =
                       new FormGroup({
                       id: new FormControl(''),
                       company: new FormControl(''),
                       name: new FormControl(''),
                       date: new FormControl(''),
                       cost: new FormControl(''),
                       state: new FormControl('')
                     });
                   }

  ngOnInit(): void {
  }

  closeDialog() { this.currentDialog.close(); }

  saveInvoices(){
    this.invoicesService.updateInvoices(this.id, this.invoicesmodel).subscribe( data =>{
        console.log(data);
        this.goToInvoicesList();
      },
      error => console.log(error));
  }

  getAllInvoices() { //Adds current data in dialog when editing row
    this.invoicesService.getAllInvoices(this.invoicesmodel_1.id).subscribe((data: Invoicesmodel[] | null) => {
      if (data == null || data.length == 0) {
        this.alertDialog("Failed to get customers");
        return;
      }
    });
  }

  initForm() {
    this.modifyInvoicesForm.setValue({
      id: this.invoicesmodel_1.id,
      company: this.invoicesmodel_1.company,
      name: this.invoicesmodel_1.name,
      date: this.invoicesmodel_1.date,
      cost: this.invoicesmodel_1.cost,
      state: this.invoicesmodel_1.state,
    });
  }

addInvoices() {
   let f: FormGroup = this.modifyInvoicesForm;

   let request: Invoicesmodel = {
     id: f.value.id,
     company: f.value.company,
     name: f.value.name,
     date: Date.now(),
     cost: f.value.cost,
     state: f.value.state,
   };

   this.invoicesService.createInvoices(request).subscribe(data=>{
     this.closeDialog();
   });
}

  alertDialog(msg: string) {}

/*  getAllInvoices() {
    this.invoicesService.getAllInvoices(this.invoicesmodel_1.id).subscribe((data: Invoicesmodel[] | null) => {
      if (data == null || data.length == 0) {
        this.alertDialog("Failed to get invoices");
        return;
      }
    });
  }*/

  goToInvoicesList(){
    this.router.navigate(['/invoices']);
  }

  onSubmit(){
    this.addInvoices();
  }
}

