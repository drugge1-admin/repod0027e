import {PictureTest} from "src/app/pictureTest";
import {Injectable} from '@angular/core';
import { HttpClient , HttpRequest, HttpEvent} from '@angular/common/http';

import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { FormBuilder, FormGroup, FormControl, ReactiveFormsModule,Validators } from '@angular/forms';




@Injectable({
  providedIn: 'root'})
export class PictureService{
pictures!: PictureTest[];

//private apiServerUrl=environment.apiBaseUrl;
private apiServerUrl = environment.apiBaseUrl;
public dataForm!:  FormGroup;
host:string = "http://localhost:8080";
choisemeny : string  = 'A';



constructor (private http: HttpClient){}

 public getPictures(): Observable<PictureTest[]>{
    return this.http.get<PictureTest[]>(`${this.apiServerUrl}/picture/all`); /**OK, getAll**/
}

  /**public List<Post> getPictures() {
          return pictureRepo.findAll();
      }**/
 get(id: any): Observable<PictureTest> {
    return this.http.get(`${this.apiServerUrl}/picture/find/${id}`);           /**OK**/
  }

list(): Observable<any> {
    return this.http.get(this.apiServerUrl);
  }
  create(data: any): Observable<any> {          /**OK**/
    return this.http.post(this.apiServerUrl, data);
  }

  update(id: any, data: any): Observable<any> {
      return this.http.put(`${this.apiServerUrl}/picture/update/${id}`, data);
    }
  /**public updatePicture(picture: PictureTest):Observable<PictureTest>{
    return this.http.put<PictureTest>(`${this.apiServerUrl}/picture/update`, picture);
    }**/

  delete(id: any): Observable<any> {
    return this.http.delete(`${this.apiServerUrl}/picture/delete/${id}`);
  }

  /**public deletePicture(pictureID: number):Observable<void>{
    return this.http.delete<void>(`${this.apiServerUrl}/picture/delete/${pictureID}`);
  }**/

  public deleteAll(): Observable<any> {
    return this.http.delete(this.apiServerUrl);
  }

 /** public findPictureByTitle(title): Observable<PictureTest[]>{
  return this.http.get<PictureTest[]>(`${this.apiServerUrl}/picture/find_1/${title}`);
}**/

public findPictureByTitle(title:string): Observable<any>{
  return this.http.get(`${this.apiServerUrl}/picture/find_1/${title}`);
}

public findPictureByCathegory(cathegory:string): Observable<any>{
  return this.http.get(`${this.apiServerUrl}/picture/find_2/${cathegory}`);
}


/**public findPictureByTitle(title:any): Observable<PictureTest[]>{
  return this.http.get <PictureTest[]> (`${this.apiServerUrl}/${title}`);
}**/

//public addPicture(picture: PictureTest):Observable<PictureTest>{
  //return this.http.post<PictureTest>(`${this.apiServerUrl}/picture/add`, picture); /**create, dubble?**/
//}

  createData(formData: FormData): Observable<any> {
    return this.http.post(`${this.apiServerUrl}/picture/add`, formData);
  }

  uploadFile(file: File): Observable<HttpEvent<{}>> {
		const formdata: FormData = new FormData();
		formdata.append('file', file);
		const req = new HttpRequest('POST', '<Server URL of the file upload>', formdata, {
			  reportProgress: true,
			  responseType: 'text'
		});

		return this.http.request(req);
   }

     getAll(): Observable<any> {

       return this.http.get(`${this.apiServerUrl}/picture/getAll`);
     }

       getData(id: number): Observable<Object> {
         return this.http.get(`${this.apiServerUrl}/picture/${id}`);
       }




}


