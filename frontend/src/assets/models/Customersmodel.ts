export interface Customersmodel {
  id: number;
  company: string | null;
  contact: string | null;
  email: string | null;
  discount: number | null;
}

export interface CustomersAddResponse {
  message: string,
  iserror: boolean
}

