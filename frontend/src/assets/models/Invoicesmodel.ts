export interface Invoicesmodel {
  id: number;
  company: string | null;
  name: string | null;
  date: any | null;
  cost: number | null;
  state: boolean | null;
}
