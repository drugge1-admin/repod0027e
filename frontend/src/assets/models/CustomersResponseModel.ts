import {Customersmodel} from "./Customersmodel";

export interface CustomersResponse{
  listOfAllCustomers: Customersmodel[];
  errorMessage: string;

}
