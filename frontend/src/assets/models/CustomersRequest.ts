export interface CustomersRequest {
  id: number | null;
  company: string | null;
  contact: string | null;
  email: string | null;
  discount: number | null
}
