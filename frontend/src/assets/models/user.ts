export interface User {
  fName?: string;
  lName?: string;
  username?: string;
  age?: number;
  mail?: string;
  isRegistered?: boolean;

}
